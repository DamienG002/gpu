#ifndef REDUCTION_H_
#define REDUCTION_H_

#include "Lock.h"
#include "Indice1D.h"

template<typename T>
__device__ void reductionIntraBlock(T* ptTabSM){
    const int TID_LOCAL=Indice1D::tidBlock();
    const int NBTHREAD_LOCAL = Indice1D::nbThreadBlock();
    int nb = NBTHREAD_LOCAL / 2;
    while(nb>=1){
        if(TID_LOCAL<nb){
            ptTabSM[TID_LOCAL]+=ptTabSM[TID_LOCAL+nb];
        }
        nb/=2;
    }

}

template<typename T>
__device__ void reductionInterBlock(T* ptDevGMResults, T* ptTabSM){
    const int TID_LOCAL=Indice1D::tidBlock();
    if(TID_LOCAL==0){
        atomicAdd(ptDevGMResults,ptTabSM[0]);
    }

}

template<typename T>
__device__ static void initialiseSM(T* tabSM, int length){
    const int NBTHREAD_BLOCK = Indice1D::nbThreadBlock();
    const int TID_LOCAL = Indice1D::tidLocal();
    int s = TID_LOCAL;
    while(s<length){
        tabSM[s] = 0.0;
        s+=NBTHREAD_BLOCK;
    }
}

#endif