#include <limits.h>
#include <curand_kernel.h>
#include "Indice1D.h"
#include "Reduction.h"
#include <curand_kernel.h>

//Nvidia Doc : Each thread gets same seed , a different sequence number , no offset


__global__ void computeMontecarloGPUMulti(float* ptrDevGM, int nTabSM, float xMin, float xMax, int m, int n, curandState* ptrDevRandom);
__global__ void setup_kernel_rand_montecarloMulti(curandState* tabGeneratorThread, int deviceId);

__device__ float uniformMulti( float min, float max, curandState& localState);
__device__ float fMulti(float x);
__device__ void reduce_intra_thread_montecarloMulti(float* tabSM, float xMin, float xMax, int m, int n, curandState* ptrDevRandom);


__global__  void setup_kernel_rand_montecarloMulti( curandState* tabGeneratorThread,int deviceId ){
    int tid = Indice1D::tid();
    // création du générateur copié du document: BilatCudaPracticalGuide_010.pdf
//Customisation du generator: Proposition (au lecteur de faire mieux)
// Contrainte : Doit etre différent d'un GPU à l'autre
    int deltaSeed=deviceId* INT_MAX;
    int deltaSequence=deviceId *100;
    int deltaOffset=deviceId *100;
    int seed=1234+deltaSeed;
    int sequenceNumber=tid+deltaSequence;
    int offset=deltaOffset;
    curand_init(seed , sequenceNumber , offset , &tabGeneratorThread[ tid ] );
}

__global__ void computeMontecarloGPUMulti(float* ptrDevGM, int nTabSM, float xMin, float xMax, int m, int n, curandState* ptrDevRandom){
    extern __shared__ float tabSM[];
    const int TID = Indice1D::tid();
    initialiseSM(tabSM, nTabSM);
    __syncthreads();
    reduce_intra_thread_montecarloMulti(tabSM, xMin, xMax, m, n, ptrDevRandom);
    __syncthreads();
    reductionIntraBlock(tabSM);
    __syncthreads();
    reductionInterBlock(ptrDevGM,tabSM);
}

__device__ void reduce_intra_thread_montecarloMulti(float* tabSM, float xMin, float xMax, int m, int n,
                                               curandState* tabGeneratorThread) {
    const int TID = Indice1D::tid();
    const int TID_LOCAL = Indice1D::tidLocal();
    const int NB_THREAD = Indice1D::nbThread();

    curandState localState = tabGeneratorThread[TID];
    float intraThreadSum = 0;
    int s = TID;
    while (s < n) {
        float xAlea = uniformMulti(xMin, xMax, localState);
        float yAlea = uniformMulti(0, m, localState);
        if (yAlea < fMulti(xAlea)) intraThreadSum++;
        s += NB_THREAD;
    }
    tabSM[TID_LOCAL] = intraThreadSum;
}


__device__ float fMulti(float x) {
    return 0.3;
}

__device__ float uniformMulti(const float MIN, const float MAX, curandState& localState) {
    float r = curand_uniform(&localState);
    return MIN + r * (MAX - MIN);
}

