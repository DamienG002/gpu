//
// Created by damien on 09.12.15.
//

#include "Device.h"
#include "cudaTools.h"
#include <iostream>
#include <curand_kernel.h>
#include <stdio.h>
#include <omp.h>

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

extern __global__ void computeMontecarloGPUMulti(float* ptrDevGM, int nTabSM, float xMin, float xMax, int m, int n, curandState* ptrDevRandom);
extern __global__ void setup_kernel_rand_montecarloMulti(curandState* ptrDevTabGeneratorThread, int deviceId);

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

__host__ bool isMontecarloMulti_Ok(int n, float xMin, float xMax, int m);

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/


/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

__host__ bool isMontecarloMulti_Ok(int n, float xMin, float xMax, int m) {
    const int NB_GPU = Device::getDeviceCount();
    assert(n%NB_GPU==0);
    float* resultPerGPU = new float[NB_GPU];
    int nbPerGPU = n/NB_GPU;

    size_t size = sizeof(float);

    dim3 dg(16, 1, 1); // disons
    dim3 db(32, 1, 1); // disons
    Device::checkDimError(dg, db);
    int nThreadPerBlock = db.x;
    size_t sizeTabSM = sizeof(float) * nThreadPerBlock;
#pragma omp parallel for
    for(int deviceId=0 ; deviceId<NB_GPU ;deviceId++)
    {
        float* ptrDevGM = NULL;
        HANDLE_ERROR(cudaSetDevice(deviceId));
// memory management
        HANDLE_ERROR(cudaMalloc(&ptrDevGM, size));
        HANDLE_ERROR(cudaMemset(ptrDevGM, 0, size));
        curandState* ptrDevRandom;
        size_t sizeRandom = sizeof(curandState) * nThreadPerBlock;
        HANDLE_ERROR(cudaMalloc((void**) &ptrDevRandom, sizeRandom));
        HANDLE_ERROR(cudaMemset(ptrDevRandom, 0, sizeRandom));

        setup_kernel_rand_montecarloMulti<<<dg,db>>>(ptrDevRandom,Device::getDeviceId());

        computeMontecarloGPUMulti<<<dg,db,sizeTabSM>>>(ptrDevGM, nThreadPerBlock, xMin, xMax, m, nbPerGPU, ptrDevRandom);
//memoryManagement
        HANDLE_ERROR(cudaMemcpy(&resultPerGPU[deviceId], ptrDevGM, size, cudaMemcpyDeviceToHost)); // sync
        HANDLE_ERROR(cudaFree(ptrDevGM));
    } // barrrière de synchronisation implicite OMP
    float resultHost = 0;
    for(int deviceId=0 ; deviceId<NB_GPU ;deviceId++)
    {
        resultHost+=resultPerGPU[deviceId];
    }
    resultHost = resultHost / n * (xMax - xMin) * m;
    printf("Montecarlo Multi GPU[n=%d, min=%f, max=%f, m=%d] = %f\n", n, xMin, xMax, m, resultHost);

    return true;
}



/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

