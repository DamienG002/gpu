#include "Indice1D.h"
#include "cudaTools.h"
#include "Reduction.h"

#include <stdio.h>


__global__ void computePiGPU(int n,float* ptrDevResult, int length, int sizeTabSM);
__device__ void reduce_intra_thread(float* tabSM, int n);
__device__ float fpi(float x);

__global__ void computePiGPU(int n, float* ptrDevRes,int length, int sizeTabSM)
{
    extern __shared__ float tabSM[];
    initialiseSM(tabSM, sizeTabSM);
    __syncthreads();
    reduce_intra_thread(tabSM, n);
    __syncthreads;
    reductionIntraBlock(tabSM);
    reductionInterBlock(ptrDevRes,tabSM);
}

__device__ void reduce_intra_thread(float* tabSM, int n){
    const int NBTHREAD = Indice1D::nbThread();
    const int TID = Indice1D::tid();
    const int TID_LOCAL = Indice1D::tidLocalBlock();
    const float DX = 1.0/(float)n;
    int s = TID;
    float sumCurrentThread = 0.0;
    float xs = 0.0;
    while(s<n){
        xs = s * DX;
        sumCurrentThread += fpi(xs);
        s+=NBTHREAD;
    }

    tabSM[TID_LOCAL] = sumCurrentThread;


}


__device__ float fpi(float x)
{
    return 4.0 / (1.0 + x * x);
}
