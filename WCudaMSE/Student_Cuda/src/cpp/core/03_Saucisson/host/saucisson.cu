//
// Created by damien on 09.12.15.
//

#include "Device.h"
#include "cudaTools.h"

#include <iostream>
#include <stdio.h>

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

extern __global__ void computePiGPU(int n,float* ptrDevResult, int length, int sizeTabSM);


/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

__host__  bool isSaucissonCuda_Ok(void);

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

__host__  static float computePi(int n);

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

__host__  bool isSaucissonCuda_Ok(int n)
{

    float sumPiGPU = computePi(n);

    printf("[Saucisson Cuda] %.10f\n",sumPiGPU);

    return true;
}


/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/
__host__ float computePi(int n)
{
    float res = 0.0;
    float* ptrRes=&res;	// host
    float* ptrDevRes; 	// device

    dim3 dg = dim3(32,1,1);  // disons
    dim3 db = dim3(128,1,1); // disons

    // Debug
    Device::checkDimError(dg,db);

    size_t size=sizeof(float);
    HANDLE_ERROR(cudaMalloc((void**) &ptrDevRes, size)); // Device memory allocation (*)
    HANDLE_ERROR(cudaMemset(ptrDevRes,0,size)) ;
    HANDLE_ERROR(cudaMemcpy(ptrRes, ptrDevRes, size, cudaMemcpyDeviceToHost));// Device -> Host
    int length = db.x;
    size_t sizeTabSM = sizeof(float)*length;
    computePiGPU<<<dg,db,sizeTabSM>>>(n, ptrDevRes,length,length); // asynchrone !!

    Device::synchronize();
    HANDLE_ERROR(cudaMemcpy(ptrRes, ptrDevRes, size, cudaMemcpyDeviceToHost));//sync
    HANDLE_ERROR(cudaFree(ptrDevRes));
    return res/(float)n;
}
/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

