//
// Created by damien on 09.12.15.
//

#include "Device.h"
#include "cudaTools.h"
#include <iostream>
#include <curand_kernel.h>
#include <stdio.h>

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

extern __global__ void computeMontecarloGPU(float* ptrDevGM, int nTabSM, float xMin, float xMax, int m, long n, curandState* ptrDevRandom);
extern __global__ void setup_kernel_rand_montecarlo(curandState* ptrDevTabGeneratorThread, int deviceId);

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

__host__ bool isMontecarlo_Ok(long n, float xMin, float xMax, int m);

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/


/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

__host__ bool isMontecarlo_Ok(long n, float xMin, float xMax, int m) {
    float resultHost = 0;
    float* ptrDevGM = NULL;
    size_t size = sizeof(float);

    dim3 dg(16, 1, 1); // disons
    dim3 db(32, 1, 1); // disons
    Device::checkDimError(dg, db);

    int nThreadPerBlock = db.x;
    size_t sizeTabSM = sizeof(float) * nThreadPerBlock;
    HANDLE_ERROR(cudaMalloc(&ptrDevGM, size));
    HANDLE_ERROR(cudaMemset(ptrDevGM, 0, size));

    curandState* ptrDevRandom;
    size_t sizeRandom = sizeof(curandState) * nThreadPerBlock;
    HANDLE_ERROR(cudaMalloc((void**) &ptrDevRandom, sizeRandom));
    HANDLE_ERROR(cudaMemset(ptrDevRandom, 0, sizeRandom));
    setup_kernel_rand_montecarlo<<<dg,db>>>(ptrDevRandom,Device::getDeviceId());

    computeMontecarloGPU<<<dg,db,sizeTabSM>>>(ptrDevGM, nThreadPerBlock, xMin, xMax, m, n, ptrDevRandom);

    HANDLE_ERROR(cudaMemcpy(&resultHost, ptrDevGM, size, cudaMemcpyDeviceToHost)); // sync
    HANDLE_ERROR(cudaFree(ptrDevGM));
    resultHost = resultHost / n * (xMax - xMin) * m;
    printf("Montecarlo[n=%d, min=%f, max=%f, m=%d] = %f\n", n, xMin, xMax, m, resultHost);

    return true;
}



/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

