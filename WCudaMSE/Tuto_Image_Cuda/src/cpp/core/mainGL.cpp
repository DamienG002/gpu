#include <iostream>
#include <stdlib.h>
#include <string.h>

#include "GLUTImageViewers.h"
#include "Viewer.h"
#include "ViewerZoomable.h"

#include "Option.h"
#include "Device.h"
#include "cudaTools.h"

#include "WarmupProvider.h"
#include "VagueProvider.h"
#include "DamierProvider.h"
#include "AdvancedProvider.h"
#include "AdvancedDomaineProvider.h"

#include "MyDisplayableProvider.h"
#include "MyGLUTWindow.h"

using std::cout;
using std::endl;
using std::string;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

int mainGL(Option& option);

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

int mainGL(Option& option)
    {
    GLUTImageViewers::init(option.getArgc(), option.getArgv());

    cout << "\n[OpenGL] mode" << endl;

    // Standard
    // Viewer : (int,int,boolean) : (px,py,isAnimation=true)
    Viewer<WarmupProvider> warmup(0, 0);
    Viewer<VagueProvider> vague(25, 25);
    ViewerZoomable<DamierProvider> damier(50, 50);
    Viewer<AdvancedProvider> advanced(75, 75);
    ViewerZoomable<AdvancedDomaineProvider> advancedDomaine(100, 100);

    // Custom (Advanced)
    Displayable_A* ptrMyDisplayable = MyDisplayableProvider::createGL();
    MyGLUTWindow myGlutWindow(ptrMyDisplayable, "OpenGL : Custom Displayable", 600, 600, 125, 125); // scene OpenGL // (w,h,px,py)

    // Common
    GLUTImageViewers::runALL(); // Bloquant, Tant qu'une fenetre est ouverte

    delete ptrMyDisplayable;

    return EXIT_SUCCESS;
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

