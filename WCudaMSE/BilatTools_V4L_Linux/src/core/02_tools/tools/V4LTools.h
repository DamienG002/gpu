#ifndef V4L2TOOLS_H_
#define V4L2TOOLS_H_

#include <string>

#include "util.h"
using std::string;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/**
 * Couche abstraite du code recuperer sur le web:
 *
 *      https://github.com/openxc/android-webcam
 *
 * But:
 *
 *    Acquistion d'image d'une webcam en utilisant l'api natif
 *
 *        video for linux (V4L)
 *
 * Note:
 *
 *   Ce code peut etre utiliser:
 *              - sous android avec NDK et binder en jni
 *              - utiliser sous linux
 *
 * Camera:
 *
 * 	Type UVL
 * 		- color
 * 		- mono chrome (example : DMM 22BUC03-ML, http://www.theimagingsource.com/fr_FR/products/oem-cameras/usb-cmos-mono/dmm22buc03ml/)
 *
 * Limitation:
 *
 *     (L1) 1 Camera utilisable a  la fois!!
 *
 * Name:
 *
 *     use le v4l2.so
 *
 */
class V4LTools
    {
	/*--------------------------------------*\
	 |*		Public			*|
	 \*-------------------------------------*/

    public:

	static bool start(int width, int height, int deviceId = 0);

	static void stop();

	/**
	 * Contraintes :
	 * 
	 *   (C1) Il faut donner 4 uchar par pixel.
	 *   (C2) ptrTabPixelRGBA must be size of w*h*4 uchar. 4 because RGBA!
	 *
	 *   Usage:
	 *
	 *        Call start() once befor,  and finally stop() when you have finish with the camera
	 */
	static void captureRGBAImage(unsigned char* ptrTabPixelRGBA, int width, int height);


	/**
	 * Contraintes :
	 *
	 *   (C1) Il faut donner 1 uchar par pixel.
	 *   (C2) ptrTabPixelRGBA must be size of w*h uchar. 4 because RGBA!
	 *
	 *   Usage:
	 *
	 *        Call start() once befor,  and finally stop() when you have finish with the camera
	 *
	 *  Camera grayscale (example)
	 *
	 *	DMM 22BUC03-ML
         *	http://www.theimagingsource.com/fr_FR/products/oem-cameras/usb-cmos-mono/dmm22buc03ml/
	 */
	static void captureGrayScaleImage(unsigned char* ptrTabPixel, int width, int height);




	/*--------------------------------------*\
	 |*		Private			*|
	 \*-------------------------------------*/

    private:

	static string createStringDeviceName(int deviceId);

    private:

	static const string DEVICE_FOLDER;
	static int DEVICE_DESCRIPTOR;

    };

#endif 

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
