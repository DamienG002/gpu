#include "../../02_tools/tools/V4LTools.h"

#include <malloc.h>
#include <sstream>

#include "capture.h"
#include "util.h"
#include "video_device.h"
#include "yuv.h"

using std::stringstream;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

int V4LTools::DEVICE_DESCRIPTOR = -1;

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

bool V4LTools::start(int width, int height, int deviceId)
    {
    string dev_name = createStringDeviceName(deviceId);

    int result = open_device(dev_name.c_str(), &DEVICE_DESCRIPTOR);
    if (result == ERROR_LOCAL)
	{
	return false;
	}

    result = init_device(DEVICE_DESCRIPTOR, width, height);

    if (result == ERROR_LOCAL)
	{
	return false;
	}

    result = start_capture(DEVICE_DESCRIPTOR);

    if (result != SUCCESS_LOCAL)
	{
	stop();
	LOGE("[V4LTools] : start : Unable to start capture, resetting device");
	}
    else
	{
	LOGE("[V4LTools] : start : Sucessfully started camera");
	}

    cache_yuv_lookup_table(YUV_TABLE);

    return result == SUCCESS_LOCAL;
    }

void V4LTools::stop()
    {
    stop_camera(&DEVICE_DESCRIPTOR);
    }

/**
 * ptrTabPixelRGBA must be size of w*h*4 char. 4 because RGBA!
 */
void V4LTools::captureRGBAImage(unsigned char* ptrTabPixelRGBA, int width, int height)
    {
    int result;

    if (!ptrTabPixelRGBA)
	{
	LOGE("[V4LTools] : captureRGBAImage : Unable to load frame, buffers not initialized");
	return;
	}

    process_camera(DEVICE_DESCRIPTOR, FRAME_BUFFERS, width, height, ptrTabPixelRGBA, IMAGE_TYPE_RGBA);
    }

void V4LTools::captureGrayScaleImage(unsigned char* ptrTabPixel, int width, int height)
    {
    int result;

    if (!ptrTabPixel)
	{
	LOGE("[V4LTools] : captureGrayScaleImage : Unable to load frame, buffers not initialized");
	return;
	}

    process_camera(DEVICE_DESCRIPTOR, FRAME_BUFFERS, width, height, ptrTabPixel, IMAGE_TYPE_RAW);
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

const string V4LTools::DEVICE_FOLDER = "/dev/";

string V4LTools::createStringDeviceName(int deviceId)
    {
    stringstream ss; //create a stringstream
    ss << DEVICE_FOLDER;
    ss << "video";
    ss << deviceId;
    return ss.str(); //return a string with the contents of the stream
    }

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

