#include <iostream>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "V4LTools.h"
#include "OpencvTools.h"

using std::cout;
using std::cerr;
using std::endl;
using namespace cv;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/**
 * only for the init blue!
 */
typedef struct
    {
	unsigned char x;
	unsigned char y;
	unsigned char z;
	unsigned char w;
    } uchar4Tools;

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

void useRGBATools(int deviceID, int w, int h);

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

static void initBlue(unsigned char* ptrPixel, int w, int h);

static bool affichageBGRA_openCV(unsigned char* ptrPixelRGBA, int w, int h);

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/**
 *  Validation :
 *
 *      Jetson:
 *
 *            Teste avec 2 camera Logitec et DMM-22BUC3-ML
 *
 * Size farmwise:
 * 	640x480		ok
 * 	1024x576	ok
 * 	1280x720 (HD)	ok
 * 	1920x1080(FHD)	ok
 *
 * Autre taille
 *
 * 	120x120		ko
 * 	800x600		ok
 * 	1024x768	ko
 *
 * KO = affichage pas valable.
 */
void useRGBATools(int deviceID, int w, int h)
    {
    cout << "[RGBA] " << endl;

    unsigned char* ptrPixel = new unsigned char[w * h * 4];

    initBlue(ptrPixel, w, h); // facultatif, permet de voir que image bleu est ecraser

    if (V4LTools::start(w, h, deviceID))
	{
	cout << "[RGBA] : Start capture ( q to quit)" << endl;

	bool isFini=false;
	while (!isFini)
	    {
	    //cout << "[RGBA] : captureRGBAImage" << endl;

	    V4LTools::captureRGBAImage(ptrPixel, w, h);

	    isFini=affichageBGRA_openCV(ptrPixel, w, h);
	    }

	V4LTools::stop();
	}
    else
	{
	cerr << "[RGBA]  Fail to start camera!" << endl;
	}

    delete[] ptrPixel;
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

void initBlue(unsigned char* ptrPixel, int w, int h)
    {
    uchar4Tools* ptrRGBA = (uchar4Tools*) ptrPixel;

    for (int i = 0; i < w * h; i++)
	{
	ptrRGBA[i].x = 0; 	//R
	ptrRGBA[i].y = 0; 	//G
	ptrRGBA[i].z = 255;     //B
	ptrRGBA[i].w = 0; 	//A
	}
    }

bool affichageBGRA_openCV(unsigned char* ptrPixelRGBA, int w, int h)
    {
    const string ID_WINDOW = "tuto webcam RGBA";

    Mat matBGRA = OpencvTools::switchRB(ptrPixelRGBA, w, h);
    bool isFini=OpencvTools::showBGR(matBGRA, ID_WINDOW);

    return isFini;
    }

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

