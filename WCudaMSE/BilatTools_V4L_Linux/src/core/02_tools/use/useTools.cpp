#include <iostream>

using std::cout;
using std::cerr;
using std::endl;


/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*	Imported		         *|
 \*-------------------------------------*/

extern void useRGBATools(int deviceID, int w, int h);
extern void useGrayTools(int deviceID, int w, int h);

/*--------------------------------------*\
|*		Public			*|
 \*-------------------------------------*/

bool useTools(int argc, char **argv, bool isMonochrome=false);

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

static void work(int deviceID, int w, int h,bool isMonochrome=false);

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/**
 * args (deviceId,w,h) :
 *
 *    0 1024 576
 */
bool useTools(int argc, char **argv,bool isMonochrome)
    {
    cout << "[Use Tools] : Use Video For Linux library Version 2 (V4l2)" << endl;

    if (argc == 4)
	{
	int deviceID = atoi(argv[1]);
	int w = atoi(argv[2]);
	int h = atoi(argv[3]);

	work(deviceID, w, h,isMonochrome);

	return true;
	}
    else
	{
	cerr << "[Use Tools] : Application need 3 arguments! deviceId Width Height" << endl;
	cerr << "[Use Tools] : Default value will be used" << endl;

	int deviceID = 0;
	int w = 640;
	int h = 480;

	work(deviceID, w, h,isMonochrome);

	return true;
	}
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

void work(int deviceID, int w, int h,bool isMonochrome)
    {
    cout << "[Use Tools] : DeviceID : " << deviceID << endl;
    cout << "[Use Tools] : Resolution (w,h) : (" << w << "," << h << ")" << endl;

    if(isMonochrome)
	{
	useGrayTools(deviceID,w,h);
	}
    else
	{
	 useRGBATools(deviceID, w, h);
	}
    }

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

