#include <iostream>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "V4LTools.h"
#include "OpencvTools.h"

using std::cout;
using std::cerr;
using std::endl;
using namespace cv;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

typedef struct
    {
	unsigned char x;
	unsigned char y;
	unsigned char z;
	unsigned char w;
    } uchar4;

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

void useGrayTools(int deviceID, int w, int h);

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

static bool affichageGrayscale_OpenCV(unsigned char* ptrPixel, int w, int h);

/*----------------------------------------------------------------------*\
 |*			Importation 					*|
 \*---------------------------------------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/**
 * Contrainte:
 *
 * 	Work only with true grayscale camera.
 *
 * Example :
 *
 *	DMM 22BUC03-ML
 *	http://www.theimagingsource.com/fr_FR/products/oem-cameras/usb-cmos-mono/dmm22buc03ml/
 *
 */
void useGrayTools(int deviceID, int w, int h)
    {
    cout << "[Monochorme]" << endl;

    unsigned char* ptrPixel = new unsigned char[w * h];

    if (V4LTools::start(w, h, deviceID))
	{
	cout << "[Monochorme] : Start capture" << endl;

	bool isFini=false;
	while (!isFini)
	    {
	    //cout << "[Monochorme] : captureGrayScaleImage" << endl;

	    V4LTools::captureGrayScaleImage(ptrPixel, w, h);

	    isFini=affichageGrayscale_OpenCV(ptrPixel, w, h);
	    }

	V4LTools::stop();
	cout << "[Monochorme] : Stop capture" << endl;
	}
    else
	{
	cerr << "[Monochorme] : Fail to start camera!" << endl;
	}

    delete[] ptrPixel;
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

bool affichageGrayscale_OpenCV(unsigned char* ptrPixel, int w, int h)
    {
    const string ID_WINDOW = "tuto webcam Grayscale";

    Mat matBGRA = OpencvTools::toGRAY(ptrPixel, w, h);
    bool isFini=OpencvTools::showBGR(matBGRA, ID_WINDOW);

    return isFini;
    }

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

