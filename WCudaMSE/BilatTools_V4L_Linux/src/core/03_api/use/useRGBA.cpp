#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

#include "V4LCaptureRGBA.h"
#include "OpencvTools.h"

using std::cout;
using std::endl;
using namespace cv;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

void useCaptureRGBA(int w, int h, int deviceID);

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

static bool affichageBGRA_CV(unsigned char* ptrPixelRGBA, int w, int h);

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

void useCaptureRGBA(int w, int h, int deviceID)
    {
    cout << "[RGBA] " << endl;

    V4LCaptureRGBA captureRGBA(w, h, deviceID);

    if (captureRGBA.start())
	{
	cout << "[RGBA] : Start capture ( q to quit)" << endl;

	unsigned char* ptrPixel = new unsigned char[w * h * 4];

	bool isFini = false;
	while (!isFini)
	    {
	    captureRGBA.captureRGBA(ptrPixel);

	    isFini = affichageBGRA_CV(ptrPixel, w, h);
	    }

	captureRGBA.stop();

	delete[] ptrPixel;
	}
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

bool affichageBGRA_CV(unsigned char* ptrPixelRGBA, int w, int h)
    {
    const string ID_WINDOW = "tuto webcam RGBA";

    Mat matBGR = OpencvTools::switchRB(ptrPixelRGBA, w, h);
    bool isFini = OpencvTools::showBGR(matBGR, ID_WINDOW);

    return isFini;
    }

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

