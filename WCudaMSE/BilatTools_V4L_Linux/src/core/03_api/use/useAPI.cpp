#include <iostream>

using std::cerr;
using std::cout;
using std::endl;


/*----------------------------------------------------------------------*\
 |*			Importation 					*|
 \*---------------------------------------------------------------------*/

extern void useCaptureRGBA(int w, int h, int deviceID);
extern void useCaptureGrayScale(int w, int h, int deviceID);

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

bool useAPI(int argc, char **argv,bool isMonochrome=false);

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

static void work(int deviceID, int w, int h,bool isMonochrome=false);

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/**
 * args (deviceId,w,h) :
 *
 *    0 1024 576
 */
bool useAPI(int argc, char **argv,bool isMonochrome)
    {
    cout << "[Use API] : Use Video For Linux library Version 2 (V4l2)" << endl;

    if (argc == 4)
	{
	int deviceID = atoi(argv[1]);
	int w = atoi(argv[2]);
	int h = atoi(argv[3]);

	work(deviceID, w, h,isMonochrome);

	return true;
	}
    else
	{
	cerr << "[Use API] : Application need 3 arguments! deviceId Width Height" << endl;
	cerr << "[Use API] : Default value will be used" << endl;

	int deviceID = 0;
	int w = 640 ;
	int h = 480;

	work(deviceID, w, h,isMonochrome);

	return true;
	}
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

void work(int deviceID, int w, int h,bool isMonochrome)
    {
    cout << "[Use Api] : DeviceID : " << deviceID << endl;
    cout << "[Use Api] : Resolution (w,h) : (" << w << "," << h << ")" << endl;

    if(isMonochrome)
	{
	useCaptureGrayScale(w,h,deviceID);
	}
    else
	{
	 useCaptureRGBA(w, h, deviceID);
	}
    }

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

