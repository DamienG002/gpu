#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

#include "V4LCaptureGrayScale.h"
#include "OpencvTools.h"

using std::cout;
using std::endl;
using namespace cv;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

void useCaptureGrayScale(int w, int h, int deviceID);

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

static bool affichageGray_CV(unsigned char* ptrPixel, int w, int h);

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/**
 * Camera:
 *
 * 	Type UVL
 * 		- mono chrome (example : DMM 22BUC03-ML, http://www.theimagingsource.com/fr_FR/products/oem-cameras/usb-cmos-mono/dmm22buc03ml/)
 *
 */
void useCaptureGrayScale(int w, int h, int deviceID)
    {
    cout << "[Monochrome] " << endl;

    V4LCaptureGrayScale captureGrayScale(w, h, deviceID);
    if (captureGrayScale.start())
	{
	cout << "[Monochrome] : Start capture ( q to quit)" << endl;

	unsigned char* ptrPixel = new unsigned char[w * h];

	bool isFini=false;
	while (!isFini)
	    {
	    captureGrayScale.captureRGBA(ptrPixel);

	    isFini= affichageGray_CV(ptrPixel, w, h);
	    }

	captureGrayScale.stop();

	delete[] ptrPixel;
	}
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/


bool affichageGray_CV(unsigned char* ptrPixel, int w, int h)
    {
    const string ID_WINDOW = "tuto webcam Grayscale";

    Mat matBGR = OpencvTools::toGRAY(ptrPixel, w, h);
    bool isFini=OpencvTools::showBGR(matBGR, ID_WINDOW);

    return isFini;
    }

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

