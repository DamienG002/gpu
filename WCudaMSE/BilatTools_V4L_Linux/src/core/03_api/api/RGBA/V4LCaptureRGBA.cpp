#include "V4LCaptureRGBA.h"

#include "V4LTools.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Constructeur		*|
 \*-------------------------------------*/

V4LCaptureRGBA::V4LCaptureRGBA(int w, int h, int deviceId) : V4LCapture_A(w,h,deviceId)
    {
    // Nothing
    }

V4LCaptureRGBA::~V4LCaptureRGBA()
    {
    // Nothing
    // destructeur classe parent appeler automatiquement.
    }

/*--------------------------------------*\
 |*		Override		*|
 \*-------------------------------------*/

/**
 * Override
 */
void V4LCaptureRGBA::captureRGBA(unsigned char* ptrPixelRGBA)
    {
    V4LTools::captureRGBAImage(ptrPixelRGBA, getW(), getH());
    }

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

