#ifndef V4L2CAPTURERGBA_H_
#define V4L2CAPTURERGBA_H_

#include "V4LCapture_A.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/**
 * But:
 *
 *    Acquistion d'image d'une webcam en utilisant l'api natif
 *
 *        video for linux (V4L)
 *
 * Note:
 *
 *   Ce code peut etre utiliser:
 *              - sous android avec NDK et binder en jni
 *              - utiliser sous linux
 *
 * Camera:
 *
 * 	Type UVL
 * 		- color
 *
 * Limitation:
 *
 *     (L1) 1 Camera utilisable a� la fois!!
 *
 * Name:
 *
 *     use le v4l2.so
 *
 */
class V4LCaptureRGBA: public V4LCapture_A
    {
	/*--------------------------------------*\
	 |*		Constructor		*|
	 \*-------------------------------------*/

    public:

	V4LCaptureRGBA(int w, int h, int deviceId = 0);

	virtual ~V4LCaptureRGBA();

	/*--------------------------------------*\
	 |*		Methodes		*|
	 \*-------------------------------------*/

    public:

	/*----------------*\
	|*  capture	  *|
	 \*--------------*/

	/**
	 * Override
	 *
	 * natural V4L
	 *
	 * Contraintes :
	 *
	 *   (C1) Il faut donner 4 uchar par pixel.
	 *   (C2) ptrTabPixelRGBA must be size of w*h*4 uchar. 4 because RGBA!
	 *   (C3) Usage: Call start() once before,  and finally stop() when you have finish with the camera
	 *
	 * Example Cuda:
	 *
	 *    int w=1280;
	 *    int h=1024;
	 *    size_t size=w*h*sizeof(uchar)*4;
	 *    V4LCaptureRGBA webcam(w,h);
	 *    webcam.start();
	 *
	 *
	 *    uchar image=cudaHostalloc(ptrTab,size);
	 *    uchar4* ptrDevImage=NULL;
	 *    HANDLE_ERROR(cudaMalloc(&ptrDevImage, size))
	 *
	 *    bool isFini=false;
	 *    while(isFini)
	 *    		{
	 *    		webcam.capture(ptrTab);
	 *
	 *    		uchar* ptrImage=(uchar4*)ptrTab;
	 *
	 *    		HANDLE_ERROR(cudaMemcpy(ptrDevImage,ptrImage, size,cudaMemcpyHostToDevice));
	 *    		myKernel<<<dg,db>>>(ptrDevImage,w,h);
	 *    		}
	 *
	 *    webcam.stop();
	 *    HANDLE_ERROR(cudaFree(ptrDevImage));
	 *    cudaHostFree(ptrTab);
	 *
	 *  Example CPP:
	 *
	 *    int w=1280;
	 *    int h=1024;
	 *    size_t size=w*h*sizeof(uchar)*4;
	 *    V4LCaptureRGBA webcam(w,h);
	 *    webcam.start();
	 *
	 *    uchar* ptrTab=new uchar[w*h*4];
	 *    bool isFini=false;
	 *    while(isFini)
	 *    		{
	 *    		webcam.capture(ptrTab);
	 *
	 *		Mat matBGR=OpencvTools::ucharRGBAtoBGRA(ptrTab,  w,  h); // opencv affiche en BGR, switch R<-->B
	 *              isFini=OpencvTools::show(matBGR,"example");
	 *    		}
	 *
	 *    webcam.stop();
	 *    delete[] ptrTab;
	 *
	 *  Other Examples:
	 *
	 *      see folder use
	 */
	virtual void captureRGBA(unsigned char* ptrPixelRGBA);



    };

#endif 

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
