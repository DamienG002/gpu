#include <iostream>

#include "V4LCapture_A.h"
#include "V4LTools.h"

using std::cerr;
using std::cout;
using std::endl;

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Constructeur			*|
 \*-------------------------------------*/

V4LCapture_A::V4LCapture_A(int w, int h, int deviceId) :
	w(w), h(h), deviceId(deviceId)
    {
    started = false;
    }

V4LCapture_A::~V4LCapture_A()
    {
    if (started)
	{
	stop();
	}
    }

/*--------------------------------------*\
 |*		Override		*|
 \*-------------------------------------*/

/**
 * Override
 */
bool V4LCapture_A::start()
    {
    if (!started)
	{
	bool isOk = V4LTools::start(w, h, deviceId);
	if (isOk)
	    {
	    started = true;
	    }
	else
	    {
	    started=false;
	    cerr << "[V4LCapture_A] : start : Error : Can't start device : " << deviceId << endl;
	    }
	return isOk;
	}
    else
	{
	cerr << "[V4LCapture_A] : start: Warning : Device already started : " << deviceId<<" : (w,h)=" << this->w << "," << this->h << ")" << endl;;
	return false;
	}

    }

/**
 * Override
 */
void V4LCapture_A::stop()
    {
    if (started)
	{
	V4LTools::stop();
	started = false;
	}
    }


/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

bool V4LCapture_A::isStarted()
    {
    return started;
    }

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

