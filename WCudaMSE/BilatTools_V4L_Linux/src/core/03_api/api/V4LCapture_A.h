#ifndef V4L2_CAPTURE_A_H_
#define V4L2_CAPTURE_A_H_

#include "Capture_I.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

class V4LCapture_A: public  Capture_I
    {
	/*--------------------------------------*\
	 |*		Constructor		*|
	 \*-------------------------------------*/
    public:

	V4LCapture_A(int w, int h, int deviceId = 0);

	virtual ~V4LCapture_A();

	/*--------------------------------------*\
	 |*		Override		*|
	 \*-------------------------------------*/

    public:



	/**
	 * Override
	 */
	virtual bool start();

	/**
	 * Override
	 */
	virtual void stop();

	/**
	 * Override
	 */
	virtual bool isStarted();

	/**
	 * Override
	 */
	virtual inline int getW()
	    {
	    return w;
	    }

	/**
	 * Override
	 */
	virtual inline int getH()
	    {
	    return h;
	    }

	/*--------------------------------------*\
	 |*		Methodes		*|
	 \*-------------------------------------*/

    public:

	inline int getDeviceId()
	    {
	    return deviceId;
	    }

	/*--------------------------------------*\
	 |*		Attributs		*|
	 \*-------------------------------------*/

    private:

	// Inputs
	int w;
	int h;
	int deviceId;

	// Tools
	bool started;

    };

#endif //V4L2_CAPTURE_A_H_
/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
