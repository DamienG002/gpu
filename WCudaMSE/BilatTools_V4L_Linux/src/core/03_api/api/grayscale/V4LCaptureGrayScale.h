#ifndef V4L2CAPTUREGRAYSCALE_H_
#define V4L2CAPTUREGRAYSCALE_H_

#include "V4LCapture_A.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/**
 * But:
 *
 *    Acquistion d'image d'une webcam en utilisant l'api natif
 *
 *        video for linux (V4L)
 *
 * Note:
 *
 *   Ce code peut etre utiliser:
 *              - sous android avec NDK et binder en jni
 *              - utiliser sous linux
 *
 * Camera:
 *
 * 	Type UVL
 * 		- mono chrome (example : DMM 22BUC03-ML, http://www.theimagingsource.com/fr_FR/products/oem-cameras/usb-cmos-mono/dmm22buc03ml/)
 *
 * Limitation:
 *
 *     (L1) 1 Camera utilisable a  la fois!!
 *
 * Name:
 *
 *     use le v4l2.so
 *
 * Example:
 *
 *      See folder use or class V4LCaptureRGBA
 */
class V4LCaptureGrayScale: public V4LCapture_A
    {
	/*--------------------------------------*\
	 |*		Constructor		*|
	 \*-------------------------------------*/

    public:

	V4LCaptureGrayScale(int w, int h, int deviceId = 0);

	virtual ~V4LCaptureGrayScale();

	/*--------------------------------------*\
	 |*		Methodes		*|
	 \*-------------------------------------*/

    public:


	/**
	 * Override
	 *
	 * Contraintes :
	 *
	 *   (C1) Il faut donner 1 uchar par pixel.
	 *   (C2) ptrTabPixelRGBA must be size of w*h uchar. 4 because RGBA!
	 *
	 *   Usage:
	 *
	 *        Call start() once befor,  and finally stop() when you have finish with the camera
	 *
	 *  Camera grayscale (example)
	 *
	 *	DMM 22BUC03-ML
         *	http://www.theimagingsource.com/fr_FR/products/oem-cameras/usb-cmos-mono/dmm22buc03ml/
         *
         *  Note
         *
         *  	nom captureRGBA de l'interface! en pratique 1 uchar par pixel, capteur true monochrome
	 */
	virtual void captureRGBA(unsigned char* ptrPixels);

    };

#endif 

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
