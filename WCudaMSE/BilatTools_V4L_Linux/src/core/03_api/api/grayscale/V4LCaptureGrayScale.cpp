#include "V4LCaptureGrayScale.h"

#include "V4LTools.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Constructeur		*|
 \*-------------------------------------*/

V4LCaptureGrayScale::V4LCaptureGrayScale(int w, int h, int deviceId) :
	V4LCapture_A(w, h, deviceId)
    {
    // Nothing
    }

V4LCaptureGrayScale::~V4LCaptureGrayScale()
    {
    // Nothing
    // destructeur classe parent appeler automatiquement.
    }

/*--------------------------------------*\
 |*		Override		*|
 \*-------------------------------------*/

/**
 * Override
 *
 *  	nom captureRGBA de l'interface! en pratique 1 uchar par pixel, capteur true monochrome
 */
void V4LCaptureGrayScale::captureRGBA(unsigned char* ptrPixels)
    {
    V4LTools::captureGrayScaleImage(ptrPixels, getW(), getH());
    }

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

