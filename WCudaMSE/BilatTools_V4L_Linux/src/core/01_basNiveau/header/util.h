#ifndef __UTIL__H__
#define __UTIL__H__

/*--------------------------------------*\
 |*		Android			*|
 \*-------------------------------------*/

#ifdef VERBOSE_V4L_LOG
#ifdef _ANDROID
#include <jni.h>
#include <android/log.h>
#define LOG_TAG "NativeWebcamJNI"
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

/*--------------------------------------*\
 |*		C			*|
 \*-------------------------------------*/

#else
#define LOGI(M,...) fprintf(stdout, "INFO %s:%d: " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#define LOGE(M,...) fprintf(stderr, "DEBUG %s:%d: " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)


#endif //__ANDROID
#else

#define LOGI(M,...)
#define LOGE(M,...)


#endif //VERBOSE


/*--------------------------------------*\
 |*		Common			*|
 \*-------------------------------------*/

#include <stdio.h>
#include <sys/ioctl.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif



#define CLEAR(x) memset(&(x), 0, sizeof(x))

#define ERROR_LOCAL -1
#define SUCCESS_LOCAL 0

int errnoexit(const char *s);

/* Private: Repeat an ioctl call until it completes and is not interrupted by a
 * a signal.
 *
 * The ioctl may still succeed or fail, so do check the return status.
 *
 * fd - the file descriptor for the ioctl.
 * request - the type of IOCTL to request.
 * arg - the target argument for the ioctl.
 *
 * Returns the status of the ioctl when it completes.
 */
int xioctl(int fd, int request, void *arg);

#ifdef __cplusplus
}
#endif


#endif // __UTIL__H__

