
#ifndef SHAPE_OVERLAY_H_
#define SHAPE_OVERLAY_H_

#include <opencv2/opencv.hpp>

/**
 * Include permettant d'include toutes l'api cv_drawer
 */
#include "shapes.h"

#include "ShapeDrawer.h"

#include "ShapeDrawerTransparant.h"

#include "LineDrawer.h"

#endif

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
