#ifndef ANIMATEUR_FONCTIONEL_H_
#define ANIMATEUR_FONCTIONEL_H_

#include "AnimableFonctionel_I.h"
#include "Chrono.h"
#include "Runnable_I.h"

using std::string;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

class AnimateurFonctionel: public Runnable_I
    {
	/*--------------------------------------*\
	|*		Constructor		*|
	 \*-------------------------------------*/

    public:

	/**
	 * Hyp:
	 * 	(H1) nbIteration suffisamment grand pour que timeElapse soit significatif
	 * 	(H2) ptrAnimable image uchar4
	 */
	AnimateurFonctionel(AnimableFonctionel_I* ptrAnimable, int nbIteration = 1000);
	virtual ~AnimateurFonctionel(void);

	/*--------------------------------------*\
	 |*		Methodes		*|
	 \*-------------------------------------*/

    public:

	/**
	 * return fps_forauto
	 */
	int run();

	/**
	 * return fps_forauto
	 */
	int getFps(void);

    private:

	void printStat(ParallelPatern parallelPatern);

	int run(ParallelPatern parallelPatern);

	/**
	 * return fps
	 */
	int start(ParallelPatern parallelPater);
	void fpsProcess();

	/*--------------------------------------*\
	 |*		Attributs		*|
	 \*-------------------------------------*/

    private:

	// Inputs
	int nbIteration;
	AnimableFonctionel_I* ptrAnimable;

	// Tools
	Chrono* ptrChrono;

	// Output
	int fps;
	float timeElapseS;

    };

#endif

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

