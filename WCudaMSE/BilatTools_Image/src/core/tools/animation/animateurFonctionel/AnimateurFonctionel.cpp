#include <iostream>
#include <math.h>

#include "AnimateurFonctionel.h"

using std::cout;
using std::endl;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported		*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

AnimateurFonctionel::AnimateurFonctionel(AnimableFonctionel_I* ptrAnimable, int nbIteration)
    {
    // Input
    this->nbIteration = nbIteration;
    this->ptrAnimable = ptrAnimable;

    // Outputs
    this->fps = -1;
    this->timeElapseS = -1;

    // tools
    this->ptrChrono = new Chrono();
    }

AnimateurFonctionel::~AnimateurFonctionel()
    {
    delete ptrChrono;
    }

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/


/**
 * fps_forauto
 */
int AnimateurFonctionel::run()
    {
    int fps_Entrelacement=run(OMP_ENTRELACEMENT); // sensiblement plus lent
    int fps_forauto=run(OMP_FORAUTO); // sensiblment plus rapide

    return fps_forauto;
    }

int AnimateurFonctionel::getFps(void)
    {
    return fps;
    }



/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

void AnimateurFonctionel::printStat(ParallelPatern parallelPatern)
    {
    cout << endl;
    cout << "[" << ptrAnimable->getTitle() << "]" << endl;
    cout << "#(w,h)          : (" << ptrAnimable->getW() << "," << ptrAnimable->getH() << ")" << endl;
    cout << "#Iteration      : " << nbIteration << endl;
    cout << "#secondes       : " << timeElapseS << " (s)" << endl;
    cout << "#ParallelPatern : " << OmpTools::toString(parallelPatern) << endl;
    cout << "#fps            : " << fps << endl;
    }


int AnimateurFonctionel::run(ParallelPatern parallelPatern)
    {
    ptrAnimable->setParallelPatern(parallelPatern);

    start(parallelPatern);
    printStat(parallelPatern);

    return fps;
    }

int AnimateurFonctionel::start(ParallelPatern parallelPatern)
    {
    const int W = ptrAnimable->getW();
    const int H = ptrAnimable->getH();
    string title = ptrAnimable->getTitle();

    cout << endl << "[" << title << "] : " << OmpTools::toString(parallelPatern) << " : fps : processing ..." << endl;

    uchar4* ptrImage = new uchar4[W * H];
    DomaineMath* domaineMath = ptrAnimable->getDomaineMathInit();

    ptrChrono->start();
    for (int i = 1; i <= nbIteration; i++)
	{
	ptrAnimable->process(ptrImage, W, H,*domaineMath);
	ptrAnimable->animationStep();
	}
    ptrChrono->stop();

    fpsProcess();
    //cout << "[" << title << "] : fps = " << fps << endl;

    delete[] ptrImage;

    return fps;
    }

void AnimateurFonctionel::fpsProcess()
    {
    this->timeElapseS = ptrChrono->getDeltaTime();

    // cout<<timeElapseS<<endl;

    this->fps=ceil(nbIteration / timeElapseS);
    }



/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

