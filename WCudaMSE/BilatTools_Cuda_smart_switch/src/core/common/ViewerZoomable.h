#ifndef VIEWER_ZOOMABLE_H_
#define VIEWER_ZOOMABLE_H_

#include <iostream>
#include "GLUTImageViewers.h"
#include "ImageFonctionel_A.h"

using std::cout;
using std::endl;
using std::string;


// see Viewer
template < class TProvider>
class ViewerZoomable
    {
  
	/*--------------------------------------*\
	|*		Constructor		*|
	\*-------------------------------------*/
	
    public:
    
	ViewerZoomable( int pxFrame, int pyFrame,bool isAnimation=true):
	    ptrImage(TProvider::createGL()),
	    glutViewer(ptrImage, isAnimation,true, pxFrame, pyFrame)
	    {
	    // rien
	    }

	~ViewerZoomable()
	    {
	    delete ptrImage; // car TProvider::createGL() fournit un pointeur d'un objet dynamique
	    }
	    
	 /*--------------------------------------*\
	|*		Methodes		*|
	 \*-------------------------------------*/
	    
	   
	/*--------------------------------------*\
	|*		Attributs		*|
	\*-------------------------------------*/
	
    private:
	      
	// Input      
	ImageFonctionel_A* ptrImage;
	
	// Tools
	GLUTImageViewers glutViewer;
	    
    };
    
    #endif

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
