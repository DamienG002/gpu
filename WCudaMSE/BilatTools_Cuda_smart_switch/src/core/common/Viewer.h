#ifndef VIEWER_H_
#define VIEWER_H_

#include <iostream>
#include "GLUTImageViewers.h"
#include "Image_A.h"

using std::cout;
using std::endl;
using std::string;


// Astuce:
//	Template :
//		Permet de travailler avec une m�thode static sans classe derivant interface
// Note:
//	Meme code pour cpu gpu :
//
// TProvider doit fournir soit:
// 	Image_A*
// 	ImageFonctionel_A*
//
// Probleme:
//	GLUTImageViewers possede 2 constructeurs:
//		- un pour Image_A
//		- un pour ImageFonctionel_A
//
//	Observation:
//		Quel type mettre pour l'attribut ptrImage?
// 	Si on met Image_A, les images fonctionelle ne seront pas zoomable!
// Solution:
//	Deux classe viewers!
//	see ViewerZoomable
template <class TProvider>
class Viewer
    {
  
	/*--------------------------------------*\
	|*		Constructor		*|
	\*-------------------------------------*/
	
    public:
    
	Viewer( int pxFrame, int pyFrame,bool isAnimation=true):
	    ptrImage(TProvider::createGL()),
	    glutViewer(ptrImage, isAnimation,false, pxFrame, pyFrame)// false pour zoom
	    {
	    // rien
	    }

	~Viewer()
	    {
	    delete ptrImage; // car TProvider::createGL() fournit un pointeur d'un objet dynamique
	    }
	    
	 /*--------------------------------------*\
	|*		Methodes		*|
	 \*-------------------------------------*/
	    
	   
	/*--------------------------------------*\
	|*		Attributs		*|
	\*-------------------------------------*/
	
    private:
	      
	// Input      
	Image_A* ptrImage;
	
	// Tools
	GLUTImageViewers glutViewer;
	    
    };
    
    #endif

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
