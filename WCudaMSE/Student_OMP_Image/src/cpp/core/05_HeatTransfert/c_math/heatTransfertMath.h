#ifndef HEAT_TRANSFERT_MATH_H_
#define HEAT_TRANSFERT_MATH_H_

#include "MathTools.h"
#include "CalibreurCudas.h"
#include "ColorTools.h"


/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

class HeatTransfertMath {

    /*--------------------------------------*\
    |*		Constructor		*|
     \*-------------------------------------*/

public:

   HeatTransfertMath() {

    }

    HeatTransfertMath(const HeatTransfertMath &source) {
        // rien
    }

    /*--------------------------------------*\
    |*		Methodes		*|
     \*-------------------------------------*/

public:


    void calculeColorTransfert(float *centralColorOutput, float colorSud, float colorEst, float colorNord,
                               float colorOuest, float k) {
        *centralColorOutput =
                *centralColorOutput + k * (colorSud + colorEst + colorNord + colorOuest - 4 * (*centralColorOutput));

    }

    void convertFloatToColor(CalibreurCudas *calibreur, float colorFloat, uchar4 *color) {
        float hue = calibreur->calibrate(colorFloat);
        ColorTools::HSB_TO_RVB(hue, color);
    }

private:



    /*--------------------------------------*\
    |*		Attributs		*|
     \*-------------------------------------*/

private:


};

#endif

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
