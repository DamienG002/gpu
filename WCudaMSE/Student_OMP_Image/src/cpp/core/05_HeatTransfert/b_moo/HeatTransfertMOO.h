#ifndef HEAT_TRANSFERT_H_
#define HEAT_TRANSFERT_H_

#include "cudaType.h"
#include "Animable_I.h"
#include "MathTools.h"
#include "heatTransfertMath.h"
#include <math.h>

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

class HeatTransfertMOO : public Animable_I {
    /*--------------------------------------*\
    |*		Constructor		*|
     \*-------------------------------------*/

public:

    HeatTransfertMOO( int w,  int h, float dt, float k);

    virtual ~HeatTransfertMOO(void);

    /*--------------------------------------*\
     |*		Methodes		*|
     \*-------------------------------------*/

public:

    /*----------------*\
    |*  Override	  *|
    \*---------------*/

    /**
     * Override
     * Call periodicly by the API
     */
    virtual void animationStep(void);

    virtual void process(uchar4 *ptrDevPixels, int w, int h);

    virtual float getAnimationPara(void);

    virtual int getW(void);
    virtual int getH(void);

    virtual string getTitle(void); // Override

    virtual void setParallelPatern(ParallelPatern parallelPatern);



    /*--------------------------------------*\
     |*		Attributs		*|
     \*-------------------------------------*/

private:

    void forAutoOMP(uchar4* ptrTabPixels, int w, int h);
    void entrelacementOMP(uchar4* ptrTabPixels, int w, int h);

    void workPixel(uchar4* ptrColorIJ, int i, int j, int s);
    void heatToScreenImageHSB(float *ptrDevImageInput, uchar4 *ptrDevImageGL, int w, int h,
                         CalibreurCudas calibreur);
    void heatEcrasement(float *ptrDevImageInput, float *ptrDevImageHeaters, float *ptrDevImageOutput, int w,
                        int h);
    void heatTransfert(float *ptrDevImageAInput, float *ptrDevImageBOutput, int w, int h, float k);

    void heatEcrasementEntr(float *ptrDevImageInput, float *ptrDevImageHeaters, float *ptrDevImageOutput, int w,
                        int h);
    void heatTransfertEntr(float *ptrDevImageAInput, float *ptrDevImageBOutput, int w, int h, float k);

    void heatEcrasementAuto(float *ptrDevImageInput, float *ptrDevImageHeaters, float *ptrDevImageOutput, int w,
                        int h);
    void heatTransfertAuto(float *ptrDevImageAInput, float *ptrDevImageBOutput, int w, int h, float k);

    void initMemory(int h, int w);

    void createHeat(int h, int w);

    void firstStep(int h, int w, float k);

    void freeMemory();

    // Inputs
     int w;
     int h;
    float dt;
    float k;

    const static int NB_ITERATION_AVEUGLE = 50;

    // Tools
    float t;
    int iteration_aveugle_counter;
    //Outputs
    string title;

    //Images memory
    float *ptrImageA;
    float *ptrImageB;
    float *prtImageHeats;

    ParallelPatern parallelPatern;

};

#endif

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
