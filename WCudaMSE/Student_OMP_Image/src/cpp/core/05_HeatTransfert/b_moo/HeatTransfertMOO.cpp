#include <iostream>
#include <assert.h>
#include <cstdio>
#include "HeatTransfertMOO.h"
#include <iostream>
#include <stdio.h>
#include <omp.h>
#include "OmpTools.h"
#include "CalibreurCudas.h"
#include <memory.h>

using std::cout;
using std::endl;
using std::string;


/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*-------------------------*\
 |*	Constructeur	    *|
 \*-------------------------*/

HeatTransfertMOO::HeatTransfertMOO(int w, int h, float dt, float k) {
    // Inputs
    this->w = w;
    this->h = h;
    this->dt = dt;
    this->k = k;

    // Tools
    this->t = 0;
    this->iteration_aveugle_counter = 0;

    //Outputs
    this->title = "[API Image OMP] : HeatTransfert OMP";

    // Check:
    assert(w == h);

    cout << endl << "[CBI] HeatTransfert dt =" << dt;

    initMemory(w,h);

    createHeat(w, h);

    firstStep(w,h,k);


}

void HeatTransfertMOO::setParallelPatern(ParallelPatern parallelPatern)
{
    this->parallelPatern = parallelPatern;
}

HeatTransfertMOO::~HeatTransfertMOO() {
    freeMemory();
}

/*-------------------------*\
 |*	Methode		    *|
 \*-------------------------*/

/**
 * Override
 * Call periodicly by the API
 */
void HeatTransfertMOO::animationStep() {
    t += dt;
}

/**
 * Override
 */
void HeatTransfertMOO::process(uchar4 *ptrDevPixels, int w, int h){

    iteration_aveugle_counter++;
    switch (parallelPatern) {

        case OMP_ENTRELACEMENT: // Plus lent sur CPU
        {
            heatTransfertEntr(ptrImageA, ptrImageB, w, h, k);
            heatEcrasementEntr(ptrImageB, prtImageHeats, ptrImageB, w, h);
            heatTransfertEntr(ptrImageB, ptrImageA, w, h, k);
            heatEcrasementEntr(ptrImageA, prtImageHeats, ptrImageA, w, h);
            break;
        }
        default:
            heatTransfertAuto(ptrImageA, ptrImageB, w, h, k);
            heatEcrasementAuto(ptrImageB, prtImageHeats, ptrImageB, w, h);
            heatTransfertAuto(ptrImageB, ptrImageA, w, h, k);
            heatEcrasementAuto(ptrImageA, prtImageHeats, ptrImageA, w, h);
            break;
    }
    CalibreurCudas calibreur = CalibreurCudas(0.0f, 1.0f, 0.7f, 0.0f);




    /*
    if (iteration_aveugle_counter == NB_ITERATION_AVEUGLE) {
        heatToScreenImageHSB(ptrImageA, ptrDevPixels, w, h, calibreur);
        iteration_aveugle_counter = 0;
    }*/
}

/*--------------*\
 |*	get	 *|
 \*--------------*/


float HeatTransfertMOO::getAnimationPara(void) {
    return t;
}

/**
 * Override
 */
int HeatTransfertMOO::getW(void) {
    return w;
}

/**
 * Override
 */
int HeatTransfertMOO::getH(void) {
    return h;
}

/**
 * Override
 */
string HeatTransfertMOO::getTitle(void) {
    return title;
}

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

//Init GPU Memory and copy data
void HeatTransfertMOO::initMemory(int h, int w) {
    const int SIZE_IMAGE = h * w * sizeof(float);
    ptrImageA = (float *) malloc(SIZE_IMAGE);
    ptrImageB = (float *) malloc(SIZE_IMAGE);
    prtImageHeats = (float *) malloc(SIZE_IMAGE);
    for(int i = 0; i < h*w; i++){
        ptrImageA[i] = 0.0;
        ptrImageB[i] = 0.0;
    }
}

void HeatTransfertMOO::createHeat(int h, int w) {
    const int SIZE_IMAGE = h * w * sizeof(float);
    float tableHostHeat[h][w];
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            tableHostHeat[i][j] = 0.0;
        }
    }
    tableHostHeat[295][400] = 0.2;
    tableHostHeat[400][295] = 0.2;
    tableHostHeat[505][400] = 0.2;
    tableHostHeat[400][505] = 0.2;

    //up cooler
    for (int i = 179; i <= 195; i++) {
        for (int j = 179; j <= 195; j++) {
            tableHostHeat[i][j] = 0.2;
        }
        for (int j = 605; j <= 621; j++) {
            tableHostHeat[i][j] = 0.2;
        }
    }

    //down cooler
    for (int i = 605; i <= 621; i++) {
        for (int j = 179; j <= 195; j++) {
            tableHostHeat[i][j] = 0.2;
        }
        for (int j = 605; j <= 621; j++) {
            tableHostHeat[i][j] = 0.2;
        }
    }

    //main heater
    for (int i = 300; i <= 500; i++) {
        for (int j = 300; j <= 500; j++) {
            tableHostHeat[i][j] = 1.0;
        }
    }

    memcpy(prtImageHeats, tableHostHeat, SIZE_IMAGE);
}

void HeatTransfertMOO::firstStep(int h, int w, float k) {

    heatEcrasement(ptrImageB, prtImageHeats, ptrImageB, w, h);
    heatTransfert(ptrImageB, ptrImageA, w, h, k);
    heatEcrasement(ptrImageA, prtImageHeats, ptrImageA, w, h);
}

void HeatTransfertMOO::freeMemory() {
    free(ptrImageA);
    free(ptrImageB);
    free(prtImageHeats);
}

//Diffusion
void HeatTransfertMOO::heatTransfert(float *ptrDevImageAInput, float *ptrDevImageBOutput, int w, int h, float k) {
    HeatTransfertMath heatTransfertMath = HeatTransfertMath();

    //const int TID = Indice2D::tid();
    //const int NB_THREAD = Indice2D::nbThread();

    const int WH = w * h;

    float color;
    float colorSud;
    float colorEst;
    float colorNord;
    float colorOuest;

    int pixelI;
    int pixelJ;

    //int s = TID;
    int s = 0;
    while (s < WH) {
        IndiceTools::toIJ(s, w, &pixelI, &pixelJ); // update (pixelI, pixelJ)
        if (pixelI != 0 && pixelJ != 0 && pixelI < h - 1 && pixelJ < w - 1) {
            int sS = IndiceTools::toS(w, pixelI - 1, pixelJ);
            int sE = IndiceTools::toS(w, pixelI, pixelJ + 1);
            int sN = IndiceTools::toS(w, pixelI + 1, pixelJ);
            int sO = IndiceTools::toS(w, pixelI, pixelJ - 1);
            color = ptrDevImageAInput[s];
            colorSud = ptrDevImageAInput[sS];
            colorEst = ptrDevImageAInput[sE];
            colorNord = ptrDevImageAInput[sN];
            colorOuest = ptrDevImageAInput[sO];
            heatTransfertMath.calculeColorTransfert(&color, colorSud, colorEst, colorNord, colorOuest, k);
            // heatTransfertMath.colorIJ(&color,pixelI, pixelJ, t); 	// update color
            ptrDevImageBOutput[s] = color;
        }

        //s += NB_THREAD;
        s++;
    }
}

void HeatTransfertMOO::heatTransfertAuto(float *ptrDevImageAInput, float *ptrDevImageBOutput, int w, int h, float k) {
    HeatTransfertMath heatTransfertMath = HeatTransfertMath();
    const int WH = w * h;
    float color;
    float colorSud;
    float colorEst;
    float colorNord;
    float colorOuest;
    int pixelI;
    int pixelJ;
#pragma omp parallel for
    for(int s = 0; s < WH; s++) {
        IndiceTools::toIJ(s, w, &pixelI, &pixelJ); // update (pixelI, pixelJ)
        if (pixelI != 0 && pixelJ != 0 && pixelI < h - 1 && pixelJ < w - 1) {
            int sS = IndiceTools::toS(w, pixelI - 1, pixelJ);
            int sE = IndiceTools::toS(w, pixelI, pixelJ + 1);
            int sN = IndiceTools::toS(w, pixelI + 1, pixelJ);
            int sO = IndiceTools::toS(w, pixelI, pixelJ - 1);
            color = ptrDevImageAInput[s];
            colorSud = ptrDevImageAInput[sS];
            colorEst = ptrDevImageAInput[sE];
            colorNord = ptrDevImageAInput[sN];
            colorOuest = ptrDevImageAInput[sO];
            heatTransfertMath.calculeColorTransfert(&color, colorSud, colorEst, colorNord, colorOuest, k);
            ptrDevImageBOutput[s] = color;
        }
    }
}

void HeatTransfertMOO::heatTransfertEntr(float *ptrDevImageAInput, float *ptrDevImageBOutput, int w, int h, float k) {
    HeatTransfertMath heatTransfertMath = HeatTransfertMath();

#pragma omp parallel
    {
        float color;
        float colorSud;
        float colorEst;
        float colorNord;
        float colorOuest;

        int pixelI;
        int pixelJ;

        const int NB_THREAD = OmpTools::getNbThread();

        const int TID = OmpTools::getTid();
        int s = TID;
        const int WH = w * h;
        while (s < WH) {
            IndiceTools::toIJ(s, w, &pixelI, &pixelJ); // update (pixelI, pixelJ)
            if (pixelI != 0 && pixelJ != 0 && pixelI < h - 1 && pixelJ < w - 1) {
                int sS = IndiceTools::toS(w, pixelI - 1, pixelJ);
                int sE = IndiceTools::toS(w, pixelI, pixelJ + 1);
                int sN = IndiceTools::toS(w, pixelI + 1, pixelJ);
                int sO = IndiceTools::toS(w, pixelI, pixelJ - 1);
                color = ptrDevImageAInput[s];
                colorSud = ptrDevImageAInput[sS];
                colorEst = ptrDevImageAInput[sE];
                colorNord = ptrDevImageAInput[sN];
                colorOuest = ptrDevImageAInput[sO];
                heatTransfertMath.calculeColorTransfert(&color, colorSud, colorEst, colorNord, colorOuest, k);
                ptrDevImageBOutput[s] = color;
            }

            s += NB_THREAD;
        }
    }
}

//Ecrasement entre les heater et le résulat des diffusion
void HeatTransfertMOO::heatEcrasementEntr(float *ptrDevImageInput, float *ptrDevImageHeaters, float *ptrDevImageOutput, int w,
                               int h) {
#pragma omp parallel
    {
        const int NB_THREAD = OmpTools::getNbThread();

        const int TID = OmpTools::getTid();
        int s = TID;
        const int WH = w * h;
        while (s < WH) {

            if (ptrDevImageHeaters[s] > 0) {
                ptrDevImageOutput[s] = ptrDevImageHeaters[s];
            } else {
                ptrDevImageOutput[s] = ptrDevImageInput[s];
            }

            s += NB_THREAD;
        }
    }
}

void HeatTransfertMOO::heatEcrasementAuto(float *ptrDevImageInput, float *ptrDevImageHeaters, float *ptrDevImageOutput, int w,
                                      int h) {

    const int WH = w * h;

#pragma omp parallel for
    for (int s = 0; s < WH; s++) {

        if (ptrDevImageHeaters[s] > 0) {
            ptrDevImageOutput[s] = ptrDevImageHeaters[s];
        } else {
            ptrDevImageOutput[s] = ptrDevImageInput[s];
        }
    }
}

void HeatTransfertMOO::heatEcrasement(float *ptrDevImageInput, float *ptrDevImageHeaters, float *ptrDevImageOutput, int w,
                                      int h) {

    const int WH = w * h;
    int s = 0;
    while (s < WH) {
        if (ptrDevImageHeaters[s] > 0) {
            ptrDevImageOutput[s] = ptrDevImageHeaters[s];
        } else {
            ptrDevImageOutput[s] = ptrDevImageInput[s];
        }
        s++;
    }
}


//Ecrasement entre les heater et le résulat des diffusion
void HeatTransfertMOO::heatToScreenImageHSB(float *ptrDevImageInput, uchar4 *ptrDevImageGL, int w, int h,
                                     CalibreurCudas calibreur) {
    HeatTransfertMath heatTransfertMath = HeatTransfertMath();

    //const int TID = Indice2D::tid();
    //const int NB_THREAD = Indice2D::nbThread();

    const int WH = w * h;

    uchar4 color;
    //int s = TID;
    int s = 0;
    while (s < WH) {

        heatTransfertMath.convertFloatToColor(&calibreur, ptrDevImageInput[s], &color);
        ptrDevImageGL[s] = color;

        //s += NB_THREAD;
        s++;
    }
}

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
