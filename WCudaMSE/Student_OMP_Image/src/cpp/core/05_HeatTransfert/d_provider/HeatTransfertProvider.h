#ifndef HEAT_TRANSFERT_PROVIDER_H_
#define HEAT_TRANSFERT_PROVIDER_H_

#include "HeatTransfertMOO.h"
#include "Image.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

class HeatTransfertProvider
    {
    public:

	static HeatTransfertMOO* createMOO(void);
	static Image* createGL(void);


    };

#endif

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

