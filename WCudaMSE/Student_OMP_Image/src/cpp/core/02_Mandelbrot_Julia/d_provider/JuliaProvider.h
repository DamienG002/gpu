//
// Created by damien on 06.10.15.
//

#ifndef STUDENT_OMP_IMAGE_JULIAPROVIDER_H
#define STUDENT_OMP_IMAGE_JULIAPROVIDER_H

#include "ImageFonctionel.h"
#include "AnimableFonctionel_I.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

class JuliaProvider
{
public:
    static ImageFonctionel* createGL(void);
    static AnimableFonctionel_I* createMOO(void);

};

#endif //STUDENT_OMP_IMAGE_JULIAPROVIDER_H
