#include "FractalMOO.h"

#include <iostream>
#include <omp.h>

#include "OmpTools.h"


#include "IndiceTools.h"

using std::cout;
using std::endl;
using std::string;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

FractalMOO::FractalMOO(unsigned int w, unsigned int h, float dt, bool julia, double c1, double c2, DomaineMath& domaineMath):variateurT(IntervalF(20, 200),dt)
    {
        this->w=w;
        this->h=h;
        this->c1 = c1;
        this->c2 = c2;
        this->julia = julia;
    //this->domaineMathInit=DomaineMath(-1.3968, -0.03362, -1.3578, 0.0013973);
    this->domaineMathInit=domaineMath;

    // Outputs
    this->title="Damier_OMP (Zoomable)";

    // Tools
    this->parallelPatern=OMP_MIXTE;

    // OMP (facultatif)
    const int NB_THREADS = OmpTools::setAndGetNaturalGranularity();
    cout << "\n[DAMIER] nbThread = " << NB_THREADS << endl;;
    }

FractalMOO::~FractalMOO(void)
    {
    // rien
    }

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/**
 * Override
 */
void FractalMOO::process(uchar4* ptrTabPixels, int w, int h, const DomaineMath& domaineMath)
    {
    switch (parallelPatern)
	{

	case OMP_ENTRELACEMENT: // Plus lent sur CPU
	    {
	    entrelacementOMP(ptrTabPixels, w, h, domaineMath);
	    break;
	    }

	case OMP_FORAUTO: // Plus rapide sur CPU
	    {
	    forAutoOMP(ptrTabPixels, w, h, domaineMath);
	    break;
	    }

	case OMP_MIXTE: // Pour tester que les deux implementations fonctionnent
	    {
	    // Note : Des saccades peuvent apparaitre � cause de la grande difference de fps entre la version entrelacer et auto
	    static bool isEntrelacement = true;
	    if (isEntrelacement)
		{
		entrelacementOMP(ptrTabPixels, w, h, domaineMath);
		}
	    else
		{
		forAutoOMP(ptrTabPixels, w, h, domaineMath);
		}
	    isEntrelacement = !isEntrelacement; // Pour swithcer a chaque iteration
	    break;
	    }
	}
    }

/**
 * Override
 */
void FractalMOO::animationStep()
    {
    variateurT.varierAndGet();
    }

/*--------------*\
 |*	get	*|
 \*-------------*/

/**
 * Override
 */
float FractalMOO::getAnimationPara()
    {
    return variateurT.get();
    }

/**
 * Override
 */
int FractalMOO::getW()
    {
    return w;
    }

/**
 * Override
 */
int FractalMOO::getH()
    {
    return h;
    }

/**
 * Override
 */
string FractalMOO::getTitle()
    {
        if(julia)
            return "Julia_OMP";
        else
            return "Mandelbrot_OMP";
    }

/*-------------*\
 |*     set	*|
 \*------------*/

void FractalMOO::setParallelPatern(ParallelPatern parallelPatern)
    {
    this->parallelPatern = parallelPatern;
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/**
 * Code entrainement Cuda
 */
void FractalMOO::entrelacementOMP(uchar4* ptrTabPixels, int w, int h, const DomaineMath& domaineMath)
    {
        FractalMath* fractalMath;
        if(julia)
            fractalMath = new JuliaMath(getAnimationPara(), c1, c2);
        else
            fractalMath = new MandelbrotMath(getAnimationPara());

        const int WH=w*h;

    #pragma omp parallel
    	{
    	const int NB_THREAD = OmpTools::getNbThread(); // dans region parallel

    	const int TID = OmpTools::getTid();
    	int s = TID; // in [0,...

    	int i;
    	int j;
    	while (s < WH)
    	    {
    	    IndiceTools::toIJ(s,w,&i,&j); // s[0,W*H[ --> i[0,H[ j[0,W[

    	    workPixel(&ptrTabPixels[s],i, j,s, domaineMath,fractalMath);

    	    s += NB_THREAD;
    	    }
    	}

    }

/**
 * Code naturel et direct OMP
 */
void FractalMOO::forAutoOMP(uchar4* ptrTabPixels, int w, int h, const DomaineMath& domaineMath)
    {
        FractalMath* fractalMath;
        if(julia)
            fractalMath = new JuliaMath(getAnimationPara(), c1, c2);
        else
            fractalMath = new MandelbrotMath(getAnimationPara());

    #pragma omp parallel for
        for (int i = 0; i < h; i++)
    	{
    	for (int j = 0; j < w; j++)
    	    {
    	    //int s = i * W + j;
    	    int s=IndiceTools::toS(w,i,j);// i[0,H[ j[0,W[  --> s[0,W*H[

    	    workPixel(&ptrTabPixels[s],i, j,s, domaineMath,fractalMath);
    	    }
    	}
    }


/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/**
 * i in [1,h]
 * j in [1,w]
 */
void FractalMOO::workPixel(uchar4* ptrColorIJ,int i, int j,int s, const DomaineMath& domaineMath,FractalMath* ptrFractalMath)
    {
    // (i,j) domaine ecran dans N2
    // (x,y) domaine math dans R2

    double x;
    double y;

    domaineMath.toXY(i, j, &x, &y); // fill (x,y) from (i,j)

    ptrFractalMath->colorXY(ptrColorIJ,x, y, domaineMath); // in [01]
    }

DomaineMath* FractalMOO::getDomaineMathInit(void)
    {
    return &domaineMathInit;
    }

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
