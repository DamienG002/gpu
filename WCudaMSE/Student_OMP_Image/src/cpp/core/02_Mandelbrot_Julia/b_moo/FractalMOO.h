#ifndef Mandelbrot_MOO_H_
#define Mandelbrot_MOO_H_

#include "cudaType.h"
#include "AnimableFonctionel_I.h"
#include "MandelbrotMath.h"
#include "JuliaMath.h"
#include "VariateurF.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

class FractalMOO: public AnimableFonctionel_I
    {

	/*--------------------------------------*\
	|*		Constructeur		*|
	 \*-------------------------------------*/

    public:

	FractalMOO(unsigned int w, unsigned int h, float dt, bool julia, double c1, double c2, DomaineMath& domaineMath);
	virtual ~FractalMOO(void);

	/*--------------------------------------*\
	|*		Methode			*|
	 \*-------------------------------------*/

    public:

	/*--------------------------------------*\
	|*	Override Animable_I		*|
	 \*-------------------------------------*/

	virtual void process(uchar4* ptrDevImageGL, int w, int h, const DomaineMath& domaineMath);
	virtual void animationStep();

	virtual float getAnimationPara();
	virtual int getW();
	virtual int getH();
	virtual string getTitle();
	virtual DomaineMath* getDomaineMathInit(void);

	virtual void setParallelPatern(ParallelPatern parallelPatern);

    private:

	void forAutoOMP(uchar4* ptrTabPixels, int w, int h, const DomaineMath& domaineMath);
	void entrelacementOMP(uchar4* ptrTabPixels, int w, int h, const DomaineMath& domaineMath);

	void workPixel(uchar4* ptrColorIJ, int i, int j, int s, const DomaineMath& domaineMath, FractalMath* ptrFractalMath);

	/*--------------------------------------*\
	|*		Attribut		*|
	 \*-------------------------------------*/

protected:

	// Inputs
	unsigned int w;
	unsigned int h;
	bool julia;
	double c1;
	double c2;
	DomaineMath domaineMathInit;

	// Outputs
	string title;

	// Tools
	VariateurF variateurT;
	ParallelPatern parallelPatern;
    };

#endif

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
