#ifndef JULIA_MATH_H_
#define JULIA_MATH_H_


#include "FractalMath.h"
/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/**
 * Dans un header only pour preparer la version cuda
 */
class JuliaMath:public FractalMath
    {

	/*--------------------------------------*\
	|*		Constructeur		*|
	 \*-------------------------------------*/

    public:

	JuliaMath(float n, double c1, double c2): FractalMath(n)
	    {
	    this->c1 = c1;
		this->c2 = c2;
	    }

	virtual ~JuliaMath(void)
	    {
	    //rien
	    }

	/*--------------------------------------*\
	|*		Methode			*|
	 \*-------------------------------------*/

    public:



	double getK(double x, double y){
		double k = 0;
		double a = x;
		double b = y;
		for(k = 0; k<=n; k++){
			double aCopy = a;
			a = (a*a-b*b)+c1;
			b = 2*aCopy*b+c2;
			if(a*a+b*b>4)
				break;
		}
		return k;
	}

    protected:

		double c1, c2;


    };

#endif

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
