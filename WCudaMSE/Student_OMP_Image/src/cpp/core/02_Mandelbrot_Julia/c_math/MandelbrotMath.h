#ifndef MANDELBROT_MATH_H_
#define MANDELBROT_MATH_H_


#include "FractalMath.h"
/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/**
 * Dans un header only pour preparer la version cuda
 */
class MandelbrotMath:public FractalMath
    {

	/*--------------------------------------*\
	|*		Constructeur		*|
	 \*-------------------------------------*/

    public:

	MandelbrotMath(float n): FractalMath(n)
	    {
	    }

	virtual ~MandelbrotMath(void)
	    {
	    //rien
	    }

	/*--------------------------------------*\
	|*		Methode			*|
	 \*-------------------------------------*/

    public:


	double getK(double x, double y){
		double k = 0;
		double a = 0;
		double b = 0;
		for(k = 0; k<=n; k++){
		    double aCopy = a;
		    a = (a*a-b*b)+x;
		    b = 2*aCopy*b+y;
		    if(a*a+b*b>4)
				break;
		}
		return k;
	    }

    };

#endif

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
