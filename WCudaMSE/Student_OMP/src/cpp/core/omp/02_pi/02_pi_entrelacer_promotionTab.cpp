#include <omp.h>
#include "00_pi_tools.h"
#include "OmpTools.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

bool isPiOMPEntrelacerPromotionTab_Ok(int n);

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

static double piOMPEntrelacerPromotionTab(int n);

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

bool isPiOMPEntrelacerPromotionTab_Ok(int n)
    {
    return isAlgoPI_OK(piOMPEntrelacerPromotionTab, n, "Pi OMP Entrelacer promotionTab");
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/**
 * pattern cuda : excellent!
 */
double piOMPEntrelacerPromotionTab(int n)
    {
    double dx = 1. / n;
    const int NB_THREAD = OmpTools::setAndGetNaturalGranularity();
    double tabSumThread[NB_THREAD];
    #pragma omp parallel
    {
	double sumThread = 0;
	double x;
	const int TID = OmpTools::getTid();
	int s = TID;
	while (s < n)
	{
	    x = s * dx;
	    sumThread += fpi(x);
	    s += NB_THREAD;
	}
	tabSumThread[TID] = sumThread;
    }
    // reduction sequancielle
    double sum = 0;
    for(int i = 0; i < NB_THREAD; i++){
	sum += tabSumThread[i];
    }
    return sum / n;
}

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

