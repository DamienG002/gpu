#ifndef CUDA_TYPE_CPU_H_
#define CUDA_TYPE_CPU_H_

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

// Attention 	: code duplicated : aussi dans shared_lib api image
// Contrainte 	: Les deux versions doivent etre identique


//coloration synthaxique ok
#ifndef uchar
   typedef unsigned char uchar;
#endif

#ifndef uint
   typedef unsigned int uint;
#endif

#ifndef ulong
   typedef unsigned long ulong;
#endif

namespace cpu
    {

    struct float2
	{
	    float x;
	    float y;
	};

    struct float3
	{
	    float x;
	    float y;
	    float z;
	};

    struct uchar4
	{
	    uchar x;
	    uchar y;
	    uchar z;
	    uchar w;
	};
    }

#endif 

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
