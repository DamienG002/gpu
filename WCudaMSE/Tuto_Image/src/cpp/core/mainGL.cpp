#include <iostream>

#include "Viewer.h"
#include "ViewerZoomable.h"
#include "GLUTImageViewers.h"
#include "Settings.h"

#include "WarmupProvider.h"
#include "VagueProvider.h"
#include "DamierProvider.h"
#include "AdvancedProvider.h"
#include "AdvancedDomaineProvider.h"

#include "MyDisplayableProvider.h"
#include "MyGLUTWindow.h"

using std::cout;
using std::endl;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported		*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

int mainGL(Settings& settings);

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

int mainGL(Settings& settings)
    {
    cout << "\n[OpenGL] mode" << endl;

    GLUTImageViewers::init(settings.getArgc(), settings.getArgv()); // call once

    // Standard
    // Viewer : (int,int,boolean) : (px,py,isAnimation=true)




    //Viewer<WarmupProvider> rippling(0, 0);
    //Viewer<VagueProvider> vague(20, 20);
    //ViewerZoomable<DamierProvider> damier(40, 40);
    //Viewer<AdvancedProvider> advanced(80, 80);
    ViewerZoomable<AdvancedDomaineProvider> advancedDomaine( 100, 100);

    // Custom : Advanced


    //Displayable_A* ptrMyDisplayable = MyDisplayableProvider::createGL();
    //MyGLUTWindow myGlutWindow(ptrMyDisplayable, "Triangle OpenGL Custom Displayable", 600, 600, 100, 100); // scene OpenGL // (w,h,px,py)

    //GLUTImageViewers::runALL();  // Bloquant, Tant qu'une fenetre est ouverte

    return EXIT_SUCCESS;
    }

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

