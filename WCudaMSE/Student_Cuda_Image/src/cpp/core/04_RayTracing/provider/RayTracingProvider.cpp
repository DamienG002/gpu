#include "RayTracingProvider.h"
#include "MathTools.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "Sphere.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*-----------------*\
 |*	static	   *|
 \*----------------*/

RayTracing* RayTracingProvider::createMOO()
{
    int dw = 16 * 60;
    int dh = 16 * 60;

    int nBall = 50;
    double bord = 200;

    double x1 = bord;
    double x2 = dw-bord;
    double y1 = bord;
    double y2 = dh-bord;
    double z1 = 10;
    double z2 = dw*2;

    double minRBall = 20;
    float maxRBall = dw/10;

    srand48(time(NULL));

    Sphere* spheres=NULL;
    spheres= (Sphere*)malloc(sizeof(Sphere)*nBall);
    for(int i = 0;i<nBall;i++)
    {
        float x = drand48();
        float y = drand48();
        float z = drand48();
        float3 centre;
        centre.x = x1+x*(x2-x1);
        centre.y = y1+y*(y2-y1);
        centre.z = z1+z*(z2-z1);
        float r = drand48();
        float hue = drand48();
        spheres[i] = Sphere(centre, minRBall+r*(maxRBall-minRBall), hue);
    }
    float dt = 0.001f;
    return new RayTracing(dw, dh, dt, x1, y1, x2, y2, spheres, nBall);
}

Image* RayTracingProvider::createGL(void)
{
    ColorRGB_01* ptrColorTitre=new ColorRGB_01(1,1,1);

    return new Image(createMOO(),ptrColorTitre);
}

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/