#include "Indice2D.h"
#include "IndiceTools.h"
#include "DomaineMath.h"
#include "cudaTools.h"
#include "Device.h"
#include "RayTracingMath.h"
#include "Sphere.h"
#include "ConstantMemoryLink.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

#define LENGTH 50 //ou const int LENGTH =2;
__constant__ Sphere TAB_DATA_CM[LENGTH];

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

__global__ void rayTracing(uchar4* ptrDevPixels, int w, int h, float t, Sphere* spheres, int n);
ConstantMemoryLink constantMemoryLink(void);

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/



/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

ConstantMemoryLink constantMemoryLink(void)
{
    Sphere* ptrDevTabData;
    size_t sizeAll = LENGTH * sizeof(Sphere);
    HANDLE_ERROR(cudaGetSymbolAddress((void **)
                                              &ptrDevTabData, TAB_DATA_CM));
    ConstantMemoryLink cmLink =
            {
                    (void**) ptrDevTabData, LENGTH, sizeAll
            };
    return cmLink;
}

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

__global__ void rayTracing(uchar4* ptrDevPixels, int w, int h, float t, Sphere* spheres, int n)
{
    RayTracingMath* rayTracingMath = new RayTracingMath();

    const int TID = Indice2D::tid();
    const int NB_THREAD = Indice2D::nbThread();

    const int WH=w*h;

    uchar4 color;

    int x;
    int y;

    int s = TID;
    while (s < WH)
    {
        IndiceTools::toIJ(s, w, &x, &y );
        rayTracingMath->colorXY(&color,x, y,t,spheres, n);
        ptrDevPixels[s] = color;
        s += NB_THREAD;
    }
    delete rayTracingMath;
}

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/