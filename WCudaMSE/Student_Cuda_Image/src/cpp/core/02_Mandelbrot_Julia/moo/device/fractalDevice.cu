#include <iostream>

#include "Indice2D.h"
#include "IndiceTools.h"
#include "cudaTools.h"
#include "Device.h"
#include "MandelbrotMath.h"
#include "JuliaMath.h"
#include "DomaineMath.h"

using std::cout;
using std::endl;


/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

__global__ void fractal(uchar4* ptrDevPixels, int w, int h, int n, bool julia, float c1, float c2, DomaineMath domaineMath);



/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

__global__ void fractal(uchar4* ptrDevPixels, int w, int h, int n, bool julia, float c1, float c2, DomaineMath domaineMath)
{

    const int TID = Indice2D::tid();
    const int NB_THREAD = Indice2D::nbThread();

    const int WH=w*h;

    uchar4 color;

    int pixelI;
    int pixelJ;

    int s = TID;
    while (s < WH) {
        IndiceTools::toIJ(s, w, &pixelI, &pixelJ); // update (pixelI, pixelJ)
        double x;
        double y;
        domaineMath.toXY(pixelI, pixelJ, &x, &y);
        if (julia) {
            JuliaMath juliaMath = JuliaMath(n, c1, c2);
            juliaMath.colorXY(&color, x, y);    // update color
        } else {
            MandelbrotMath mandelbrotMath = MandelbrotMath(n);
            mandelbrotMath.colorXY(&color, x, y);    // update color
        }
        ptrDevPixels[s] = color;

        s += NB_THREAD;
    }
}

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
