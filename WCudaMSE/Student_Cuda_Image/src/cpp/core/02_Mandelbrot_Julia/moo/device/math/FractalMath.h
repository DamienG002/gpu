#ifndef FRACTAL_MATH_H_
#define FRACTAL_MATH_H_

#include <math.h>
#include "CalibreurF.h"
#include "ColorTools.h"
/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/**
 * Dans un header only pour preparer la version cuda
 */
class FractalMath
{

    /*--------------------------------------*\
    |*		Constructeur		*|
     \*-------------------------------------*/

public:

    __device__ FractalMath(float n): calibreur(IntervalF(0, n), IntervalF(0, 1))
    {
        this->n = n;
    }

    __device__ virtual ~FractalMath(void)
    {
        //rien
    }

    /*--------------------------------------*\
    |*		Methode			*|
     \*-------------------------------------*/

public:

    __device__ virtual double getK(double x, double y)=0;

    __device__ void colorXY(uchar4* ptrColor, double x, double y)
    {
        float k = this->getK(x,y);
        if(k>=n){
            ptrColor->x = 0;
            ptrColor->y = 0;
            ptrColor->z = 0;
            ptrColor->w = 255;
        }else{
            calibreur.calibrer(k);
            double hue01 = k;
            ColorTools::HSB_TO_RVB(hue01, ptrColor); // update color
            ptrColor->w = 255;
        }
    }

protected:

    // Inputs
    float n;

    // Tools
    CalibreurF calibreur;


};

#endif

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
