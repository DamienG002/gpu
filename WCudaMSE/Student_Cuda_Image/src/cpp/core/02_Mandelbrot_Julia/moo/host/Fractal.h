#ifndef FRACTAL_H_
#define FRACTAL_H_

#include "cudaTools.h"
#include "AnimableFonctionel_I.h"
#include "MathTools.h"
#include "VariateurF.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

class Fractal: public AnimableFonctionel_I
{
    /*--------------------------------------*\
    |*		Constructor		*|
     \*-------------------------------------*/

public:

    Fractal(int w, int h, float dt, bool julia, double c1, double c2, DomaineMath& domaineMath);
    virtual ~Fractal(void);

    /*--------------------------------------*\
     |*		Methodes		*|
     \*-------------------------------------*/

public:

    /*-------------------------*\
    |*   Override Animable_I   *|
     \*------------------------*/

    /**
     * Call periodicly by the api
     */
    virtual void process(uchar4* ptrDevPixels, int w, int h, const DomaineMath& domaineMath);
    /**
     * Call periodicly by the api
     */
    virtual void animationStep();

    virtual float getAnimationPara();
    virtual string getTitle();
    virtual int getW();
    virtual int getH();
    virtual DomaineMath* getDomaineMathInit(void);

private:

    /*--------------------------------------*\
     |*		Attributs		*|
     \*-------------------------------------*/

private:

    // Inputs
    int w;
    int h;
    float dt;
    bool julia;
    double c1;
    double c2;
    DomaineMath domaineMath;

    VariateurF variateurT;


    // Tools
    dim3 dg;
    dim3 db;

    //Outputs
    string title;
};

#endif

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
