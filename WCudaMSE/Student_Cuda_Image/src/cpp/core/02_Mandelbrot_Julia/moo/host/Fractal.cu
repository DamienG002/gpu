#include <assert.h>

#include "Fractal.h"
#include "Device.h"
#include "MathTools.h"

using cpu::IntervalF;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

extern __global__ void fractal(uchar4* ptrDevPixels, int w, int h, int n, bool julia, float c1, float c2, DomaineMath domaineMath);

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*-------------------------*\
 |*	Constructeur	    *|
 \*-------------------------*/

Fractal::Fractal(int w, int h, float dt, bool julia, double c1, double c2, DomaineMath& domaineMath):variateurT(IntervalF(20, 200),dt) {

    // Inputs
    this->w = w;
    this->h = h;
    this->julia = julia;
    this->c1 = c1;
    this->c2 = c2;

    // Tools
    this->dg = dim3(64, 64, 1); // disons a optimiser
    this->db = dim3(32, 32, 1); // disons a optimiser


    // Outputs
    this->title = "Fractal_Cuda";

    this->domaineMath = domaineMath;

    //print(dg, db);
    Device::assertDim(dg, db);
}

Fractal::~Fractal()
{
    // rien
}

/*-------------------------*\
 |*	Methode		    *|
 \*-------------------------*/


/**
 * Override
 */
void Fractal::process(uchar4* ptrDevPixels, int w, int h, const DomaineMath& domaineMath)
{
    fractal<<<dg,db>>>(ptrDevPixels,w,h,getAnimationPara(),julia,c1,c2, domaineMath);
}


/**
 * Override
 */
void Fractal::animationStep()
{
    variateurT.varierAndGet();
}

/*--------------*\
 |*	get	 *|
 \*--------------*/

/**
 * Override
 */
float Fractal::getAnimationPara(void)
{
    return variateurT.get();
}

/**
 * Override
 */
int Fractal::getW(void)
{
    return w;
}

/**
 * Override
 */
int Fractal::getH(void)
{
    return  h;
}

/**
 * Override
 */
string Fractal::getTitle(void)
{
    return title;
}

DomaineMath* Fractal::getDomaineMathInit(void)
{
    return &domaineMath;
}


/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

