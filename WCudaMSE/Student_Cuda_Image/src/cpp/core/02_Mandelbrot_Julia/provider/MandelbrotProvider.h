#ifndef MANDELBROT_PROVIDER_H_
#define MANDELBROT_PROVIDER_H_

#include "Fractal.h"
#include "ImageFonctionel.h"
#include "AnimableFonctionel_I.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

class MandelbrotProvider
{
public:

    static AnimableFonctionel_I* createMOO(void);
    static ImageFonctionel* createGL(void);

};

#endif

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

