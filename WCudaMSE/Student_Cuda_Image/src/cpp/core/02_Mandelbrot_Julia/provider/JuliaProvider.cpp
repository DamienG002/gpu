#include "JuliaProvider.h"

AnimableFonctionel_I* JuliaProvider::createMOO()
{
    int dw = 16 * 60; // =32*30=960
    int dh = 16 * 60; // =32*30=960
    double c1 = -0.12;
    double c2 = 0.85;
    float dt = 1;

    DomaineMath domaineMath = DomaineMath(-1.3, -1.4, 1.3, 1.4);

    return new Fractal(dw, dh, dt, true, c1, c2, domaineMath);
}

ImageFonctionel* JuliaProvider::createGL(void)
{
    return new ImageFonctionel(createMOO());
}