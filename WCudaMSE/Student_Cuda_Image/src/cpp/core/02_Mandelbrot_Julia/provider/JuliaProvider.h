#ifndef JULIA_PROVIDER_H_
#define JULIA_PROVIDER_H_

#include "Fractal.h"
#include "ImageFonctionel.h"
#include "AnimableFonctionel_I.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

class JuliaProvider
{
public:

    static AnimableFonctionel_I* createMOO(void);
    static ImageFonctionel* createGL(void);

};

#endif

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

