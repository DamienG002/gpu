#include "MandelbrotProvider.h"

AnimableFonctionel_I* MandelbrotProvider::createMOO()
{
    int dw = 16 * 60; // =32*30=960
    int dh = 16 * 60; // =32*30=960
    float dt = 1;

    DomaineMath domaineMath = DomaineMath(-2.1, -1.3, 0.8, 1.3);

    return new Fractal(dw, dh, dt, false, 0, 0, domaineMath);
}

ImageFonctionel* MandelbrotProvider::createGL(void)
{
    return new ImageFonctionel(createMOO());
}