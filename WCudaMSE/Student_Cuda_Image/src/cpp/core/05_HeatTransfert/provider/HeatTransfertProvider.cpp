#include "HeatTransfertProvider.h"
#include "MathTools.h"
#include "HeatTransfert.h"
/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*-----------------*\
 |*	static	   *|
 \*----------------*/

HeatTransfert* HeatTransfertProvider::createMOO(int dg, int db)
    {
    float dt = 1;
    int dw =800;
    int dh = 800;
    float k = 0.25;

    return new HeatTransfert(dw, dh, dt,k, dg, db);
    }

 Image* HeatTransfertProvider::createGL(void)
     {
         ColorRGB_01* ptrColorTitre=new ColorRGB_01(1,1,1);

         return new Image(createMOO(8,8),ptrColorTitre);
     }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
