#include <iostream>
#include <assert.h>
#include <cstdio>
#include "HeatTransfert.h"
#include "Device.h"
#include "MathTools.h"
#include "CalibreurCudas.h"
#include "Indice2D.h"
#include "IndiceTools.h"
#include "cudaTools.h"
#include "Device.h"
#include "cstdio"
#include "heatTransfertMath.h"

using std::cout;
using std::endl;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

texture<float,2,cudaReadModeElementType> textureImgA;
texture<float,2,cudaReadModeElementType> textureImgB;

extern __global__ void heatTransfertFromA(float *ptrDevImageBOutput, int w, int h, float k);

extern __global__ void heatTransfertFromB(float *ptrDevImageBOutput, int w, int h, float k);

extern __global__ void heatEcrasement(float *ptrDevImageInput, float *ptrDevImageHeaters, float *ptrDevImageOutput,
                                      int w, int h);

extern __global__ void heatToScreenImageHSB(float *ptrDevImageInput, uchar4 *ptrDevImageGL, int w, int h,
                                            CalibreurCudas calibreur);

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*-------------------------*\
 |*	Constructeur	    *|
 \*-------------------------*/

HeatTransfert::HeatTransfert(int w, int h, float dt, float k, int dg, int db) {
    // Inputs
    this->w = w;
    this->h = h;
    this->dt = dt;
    this->k = k;

    // Tools
    this->dg = dim3(8, dg, 1); // disons a optimiser
    this->db = dim3(32, db, 1); // disons a optimiser
    this->t = 0;
    this->iteration_aveugle_counter = 0;

    //Outputs
    this->title = "[API Image Cuda] : HeatTransfert CUDA";

    // Check:
    //print(dg, db);
    Device::assertDim(dg, db);
    assert(w == h);

    cout << endl << "[CBI] HeatTransfert dt =" << dt;

    initGPUMemory(h, w);
    createDataForGPU(h, w);
    createTex(h,w);
    initGPUFirstStep(h, w, k);

}

HeatTransfert::~HeatTransfert() {
    HANDLE_ERROR (cudaUnbindTexture(textureImgA));
    HANDLE_ERROR (cudaUnbindTexture(textureImgB));
    freeGPUMemory();
}

/*-------------------------*\
 |*	Methode		    *|
 \*-------------------------*/

/**
 * Override
 * Call periodicly by the API
 */
void HeatTransfert::animationStep() {
    t += dt;
}

/**
 * Override
 */
void HeatTransfert::process(uchar4 *ptrDevPixels, int w, int h){
    CalibreurCudas calibreur = CalibreurCudas(0.0f, 1.0f, 0.7f, 0.0f);
    iteration_aveugle_counter++;

    heatTransfertFromA<<<dg, db>>>(ptrImageDeviceB, w, h, k);
    Device::synchronize();
    heatEcrasement<<<dg, db>>>(ptrImageDeviceB, prtImageHeats, ptrImageDeviceB, w, h);
    Device::synchronize();
    heatTransfertFromB<<< dg, db >>>(ptrImageDeviceA, w, h, k);
    Device::synchronize();
    heatEcrasement<<<dg, db>>>(ptrImageDeviceA, prtImageHeats, ptrImageDeviceA, w, h);
    Device::synchronize();


    if (iteration_aveugle_counter == NB_ITERATION_AVEUGLE) {
        heatToScreenImageHSB<<<dg, db >>>(ptrImageDeviceA, ptrDevPixels, w, h, calibreur);
        Device::synchronize();
        iteration_aveugle_counter = 0;
    }
}

void HeatTransfert::createTex(int h, int w){
    textureImgA.filterMode= cudaFilterModePoint;
    textureImgA.normalized=false;
    textureImgA.addressMode[0] = cudaAddressModeClamp ;
    textureImgA.addressMode[1] = cudaAddressModeClamp ;

    textureImgB.filterMode= cudaFilterModePoint;
    textureImgB.normalized=false;
    textureImgB.addressMode[0] = cudaAddressModeClamp ;
    textureImgB.addressMode[1] = cudaAddressModeClamp ;

    size_t pitch = w * sizeof(float); //size ligne
    cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<float>();

    HANDLE_ERROR (cudaBindTexture2D( NULL,textureImgA,ptrImageDeviceA,channelDesc,w,h,pitch ) );
    HANDLE_ERROR (cudaBindTexture2D( NULL,textureImgB,ptrImageDeviceB,channelDesc,w,h,pitch ) );

}


/*--------------*\
 |*	get	 *|
 \*--------------*/


float HeatTransfert::getAnimationPara(void) {
    return t;
}

/**
 * Override
 */
int HeatTransfert::getW(void) {
    return w;
}

/**
 * Override
 */
int HeatTransfert::getH(void) {
    return h;
}

/**
 * Override
 */
string HeatTransfert::getTitle(void) {
    return title;
}

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

//Init GPU Memory and copy data
void HeatTransfert::initGPUMemory(int h, int w) {
    const int SIZE_IMAGE = h * w * sizeof(float);
    HANDLE_ERROR(cudaMalloc(&ptrImageDeviceA, SIZE_IMAGE));
    HANDLE_ERROR(cudaMemset(ptrImageDeviceA, 0, SIZE_IMAGE));
    HANDLE_ERROR(cudaMalloc(&ptrImageDeviceB, SIZE_IMAGE));
    HANDLE_ERROR(cudaMemset(ptrImageDeviceB, 0, SIZE_IMAGE));
    HANDLE_ERROR(cudaMalloc(&prtImageHeats, SIZE_IMAGE));
    HANDLE_ERROR(cudaMemset(prtImageHeats, 0, SIZE_IMAGE));
}

void HeatTransfert::createDataForGPU(int h, int w) {
    const int SIZE_IMAGE = h * w * sizeof(float);
    float tableHostHeat[h][w];
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            tableHostHeat[i][j] = 0.0;
        }
    }
    tableHostHeat[295][400] = 0.2;
    tableHostHeat[400][295] = 0.2;
    tableHostHeat[505][400] = 0.2;
    tableHostHeat[400][505] = 0.2;

    //up cooler
    for (int i = 179; i <= 195; i++) {
        for (int j = 179; j <= 195; j++) {
            tableHostHeat[i][j] = 0.2;
        }
        for (int j = 605; j <= 621; j++) {
            tableHostHeat[i][j] = 0.2;
        }
    }

    //down cooler
    for (int i = 605; i <= 621; i++) {
        for (int j = 179; j <= 195; j++) {
            tableHostHeat[i][j] = 0.2;
        }
        for (int j = 605; j <= 621; j++) {
            tableHostHeat[i][j] = 0.2;
        }
    }

    //main heater
    for (int i = 300; i <= 500; i++) {
        for (int j = 300; j <= 500; j++) {
            tableHostHeat[i][j] = 1.0;
        }
    }

    HANDLE_ERROR(cudaMemcpy(prtImageHeats, tableHostHeat, SIZE_IMAGE, cudaMemcpyHostToDevice)); //barriere implicite de sync
}

void HeatTransfert::initGPUFirstStep(int h, int w, float k) {

    heatEcrasement<<<dg, db>>>(ptrImageDeviceB, prtImageHeats, ptrImageDeviceB, w, h);
    Device::synchronize();
    heatTransfertFromB<<<dg, db>>>(ptrImageDeviceA, w, h, k);
    Device::synchronize();
    heatEcrasement<<<dg, db>>>(ptrImageDeviceA, prtImageHeats, ptrImageDeviceA, w, h);
    Device::synchronize();

}

void HeatTransfert::freeGPUMemory() {
    HANDLE_ERROR(cudaFree(ptrImageDeviceA));
    HANDLE_ERROR(cudaFree(ptrImageDeviceB));
    HANDLE_ERROR(cudaFree(prtImageHeats));
}

__global__ void heatTransfertFromA(float *ptrDevImageBOutput, int w, int h, float k) {

    HeatTransfertMath heatTransfertMath = HeatTransfertMath();
    const int TID = Indice2D::tid();
    const int NB_THREAD = Indice2D::nbThread();
    const int WH = w * h;
    float color;
    int pixelI;
    int pixelJ;
    int s = TID;
    while (s < WH) {
        IndiceTools::toIJ(s, w, &pixelI, &pixelJ); // update (pixelI, pixelJ)
        color = tex2D(textureImgA,pixelI, pixelJ);
        /*colorSud = tex2D(textureImgA,pixelI - 1, pixelJ);
        colorEst = tex2D(textureImgA,pixelI, pixelJ + 1);
        colorNord = tex2D(textureImgA,pixelI + 1, pixelJ);
        colorOuest = tex2D(textureImgA,pixelI, pixelJ - 1);*/
        heatTransfertMath.calculeColorTransfert(&color, tex2D(textureImgA,pixelI - 1, pixelJ), tex2D(textureImgA,pixelI, pixelJ + 1),
                                                tex2D(textureImgA,pixelI + 1, pixelJ), tex2D(textureImgA,pixelI, pixelJ - 1), k);
        ptrDevImageBOutput[s] = color;
        s += NB_THREAD;
    }
}

__global__ void heatTransfertFromB(float *ptrDevImageBOutput, int w, int h, float k) {

    HeatTransfertMath heatTransfertMath = HeatTransfertMath();
    const int TID = Indice2D::tid();
    const int NB_THREAD = Indice2D::nbThread();
    const int WH = w * h;
    float color;
    int pixelI;
    int pixelJ;
    int s = TID;
    while (s < WH) {
        IndiceTools::toIJ(s, w, &pixelI, &pixelJ); // update (pixelI, pixelJ)
        color = tex2D(textureImgB,pixelI, pixelJ);
        heatTransfertMath.calculeColorTransfert(&color, tex2D(textureImgB,pixelI - 1, pixelJ), tex2D(textureImgB,pixelI, pixelJ + 1),
                                                tex2D(textureImgB,pixelI + 1, pixelJ), tex2D(textureImgB,pixelI, pixelJ - 1), k);
        ptrDevImageBOutput[s] = color;
        s += NB_THREAD;
    }
}

//Ecrasement entre les heater et le résulat des diffusion
__global__ void heatEcrasement(float *ptrDevImageInput, float *ptrDevImageHeaters, float *ptrDevImageOutput, int w,
                               int h) {
    HeatTransfertMath heatTransfertMath = HeatTransfertMath();

    const int TID = Indice2D::tid();
    const int NB_THREAD = Indice2D::nbThread();
    const int WH = w * h;

    int s = TID;
    while (s < WH) {
        bool b = ptrDevImageHeaters[s];
        int i = b;
        ptrDevImageOutput[s] = (1-i)*ptrDevImageInput[s]+i*ptrDevImageHeaters[s];
        s += NB_THREAD;
    }
}


//Ecrasement entre les heater et le résulat des diffusion
__global__ void heatToScreenImageHSB(float *ptrDevImageInput, uchar4 *ptrDevImageGL, int w, int h,
                                     CalibreurCudas calibreur) {
    HeatTransfertMath heatTransfertMath = HeatTransfertMath();
    const int TID = Indice2D::tid();
    const int NB_THREAD = Indice2D::nbThread();
    const int WH = w * h;
    uchar4 color;
    int s = TID;
    while (s < WH) {
        heatTransfertMath.convertFloatToColor(&calibreur, ptrDevImageInput[s], &color);
        ptrDevImageGL[s] = color;
        s += NB_THREAD;
    }
}

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
