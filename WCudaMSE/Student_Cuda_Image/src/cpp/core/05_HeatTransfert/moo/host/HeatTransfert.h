#ifndef HEAT_TRANSFERT_H_
#define HEAT_TRANSFERT_H_

#include "cudaTools.h"
#include "Animable_I.h"
#include "MathTools.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

class HeatTransfert : public Animable_I {
    /*--------------------------------------*\
    |*		Constructor		*|
     \*-------------------------------------*/

public:

    HeatTransfert(int w, int h, float dt, float k, int dg, int db);

    virtual ~HeatTransfert(void);

    /*--------------------------------------*\
     |*		Methodes		*|
     \*-------------------------------------*/

public:

    /*----------------*\
    |*  Override	  *|
    \*---------------*/

    /**
     * Override
     * Call periodicly by the API
     */
    virtual void animationStep(void);

    virtual void process(uchar4 *ptrDevPixels, int w, int h);

    virtual float getAnimationPara(void);

    virtual int getW(void);
    virtual int getH(void);

    virtual string getTitle(void); // Override

    /*--------------------------------------*\
     |*		Attributs		*|
     \*-------------------------------------*/

private:

    void initGPUMemory(int h, int w);

    void createDataForGPU(int h, int w);

    void initGPUFirstStep(int h, int w, float k);

    void freeGPUMemory();

    void createTex(int h, int w);

    // Inputs
    int w;
    int h;
    float dt;
    float k;

    const static int NB_ITERATION_AVEUGLE = 50;

    // Tools
    dim3 dg;
    dim3 db;
    float t;
    int iteration_aveugle_counter;
    //Outputs
    string title;

    //Images memory
    float *ptrImageDeviceA;
    float *ptrImageDeviceB;
    float *prtImageHeats;

};

#endif

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
