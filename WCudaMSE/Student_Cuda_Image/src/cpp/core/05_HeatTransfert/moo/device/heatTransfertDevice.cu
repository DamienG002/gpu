/*#include "Indice2D.h"
#include "IndiceTools.h"
#include "cudaTools.h"
#include "Device.h"
#include "CalibreurCudas.h"
#include "cstdio"
#include "ConstantMemoryLink.h"

#include "heatTransfertMath.h"*/



/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

//__global__ void heatTransfert(float *ptrDevImageAInput, float *ptrDevImageBOutput, int w, int h, float k);

//__global__ void heatEcrasement(float *ptrDevImageInput, float *ptrDevImageHeaters, float *ptrDevImageOutput, int w, int h);

//__global__ void heatToScreenImageHSB(float *ptrDevImageInput, uchar4 *ptrDevImageGL, int w, int h, CalibreurCudas calibreur);

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

//Diffusion
/*__global__ void heatTransfert(float *ptrDevImageAInput, float *ptrDevImageBOutput, int w, int h, float k) {
    texImg.filterMode= cudaFilterModePoint;
    texImg.normalized=false;
    texImg.addressMode[0] = cudaAddressModeClamp ;
    texImg.addressMode[1] = cudaAddressModeClamp ;

    size_t pitch = w * sizeof(float); //size ligne
    cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<float>();

    HANDLE_ERROR (cudaBindTexture2D( NULL,texImg,ptrDevImageAInput,channelDesc,w,h,pitch ) );

    HANDLE_ERROR (cudaUnbindTexture(textureInput));

    HeatTransfertMath heatTransfertMath = HeatTransfertMath();
    const int TID = Indice2D::tid();
    const int NB_THREAD = Indice2D::nbThread();
    const int WH = w * h;
    float color;
    float colorSud;
    float colorEst;
    float colorNord;
    float colorOuest;
    int pixelI;
    int pixelJ;
    int s = TID;
    while (s < WH) {
        IndiceTools::toIJ(s, w, &pixelI, &pixelJ); // update (pixelI, pixelJ)
        if (pixelI != 0 && pixelJ != 0 && pixelI < h - 1 && pixelJ < w - 1) {
            int sS = IndiceTools::toS(w, pixelI - 1, pixelJ);
            int sE = IndiceTools::toS(w, pixelI, pixelJ + 1);
            int sN = IndiceTools::toS(w, pixelI + 1, pixelJ);
            int sO = IndiceTools::toS(w, pixelI, pixelJ - 1);
            color = ptrDevImageAInput[s];
            colorSud = ptrDevImageAInput[sS];
            colorEst = ptrDevImageAInput[sE];
            colorNord = ptrDevImageAInput[sN];
            colorOuest = ptrDevImageAInput[sO];
            heatTransfertMath.calculeColorTransfert(&color, colorSud, colorEst, colorNord, colorOuest, k);
            ptrDevImageBOutput[s] = color;
        }

        s += NB_THREAD;
    }
}

//Ecrasement entre les heater et le résulat des diffusion
__global__ void heatEcrasement(float *ptrDevImageInput, float *ptrDevImageHeaters, float *ptrDevImageOutput, int w,
                               int h) {
    HeatTransfertMath heatTransfertMath = HeatTransfertMath();

    const int TID = Indice2D::tid();
    const int NB_THREAD = Indice2D::nbThread();
    const int WH = w * h;

    int s = TID;
    while (s < WH) {
        bool b = ptrDevImageHeaters[s];
        int i = b;
        ptrDevImageOutput[s] = (1-i)*ptrDevImageInput[s]+i*ptrDevImageHeaters[s];
        s += NB_THREAD;
    }
}


//Ecrasement entre les heater et le résulat des diffusion
__global__ void heatToScreenImageHSB(float *ptrDevImageInput, uchar4 *ptrDevImageGL, int w, int h,
                                     CalibreurCudas calibreur) {
    HeatTransfertMath heatTransfertMath = HeatTransfertMath();
    const int TID = Indice2D::tid();
    const int NB_THREAD = Indice2D::nbThread();
    const int WH = w * h;
    uchar4 color;
    int s = TID;
    while (s < WH) {
        heatTransfertMath.convertFloatToColor(&calibreur, ptrDevImageInput[s], &color);
        ptrDevImageGL[s] = color;
        s += NB_THREAD;
    }
}*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

